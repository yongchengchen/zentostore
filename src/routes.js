import React from 'react';
// import Loadable from "react-loadable";

const Home = React.lazy(() => import('./pages/Home'));
const SignIn = React.lazy(() => import('./pages/SignIn'));
const SignUp = React.lazy(() => import('./pages/SignUp'));
const CategorySearchPage = React.lazy(() => import('./pages/CategorySearchPage'));
const ProductPage = React.lazy(() => import('./pages/ProductPage'));
const CheckoutPage = React.lazy(() => import('./pages/CheckoutPage'));
const ShoppingCart = React.lazy(() => import('./pages/ShoppingCart'));
const CheckoutSuccess = React.lazy(() => import('./pages/CheckoutSuccess'));
// const PaymentCallback = React.lazy(() => import('./pages/PaymentCallback'));

const routes = [{
    path: '/',
    name: 'Home',
    is_regx: false,
    component: Home,
    helmet: {
      title: "AlphaZento - opensource e-commerce platform"
    }
  },
  {
    path: '/signin',
    name: 'Sign In',
    is_regx: false,
    component: SignIn,
    helmet: {
      title: "SignIn - opensource e-commerce platform"
    }
  },
  {
    path: '/signup',
    name: 'Sign Up',
    is_regx: false,
    component: SignUp,
    helmet: {
      title: "SignUp - opensource e-commerce platform"
    }
  },
  {
    path: '/cart',
    name: 'ShoppingCart',
    is_regx: false,
    component: ShoppingCart,
    helmet: {
      title: "Shopping Cart AlphaZento"
    }
  },
  {
    path: "/checkout",
    name: 'Checkout',
    is_regx: false,
    component: CheckoutPage,
    helmet: {
      title: "Checkout -- AlphaZento"
    }
  },
  {
    path: "/search",
    name: 'Search',
    is_regx: false,
    component: CategorySearchPage,
    helmet: {
      title: "Search Page -- AlphaZento"
    }
  },
  {
    path: "/category/:id",
    name: 'Category',
    is_regx: true,
    component: CategorySearchPage,
    helmet: {
      title: "Category Page -- AlphaZento"
    }
  },
  {
    path: "/product/:id",
    name: 'Product',
    is_regx: true,
    component: ProductPage,
    helmet: {
      title: "Product Page -- AlphaZento"
    }
  },
  {
    path: "/checkout/success",
    name: 'CheckoutSuccess',
    is_regx: false,
    component: CheckoutSuccess,
    helmet: {
      title: "CheckoutSuccess -- AlphaZento"
    }
  }

  // {
  //   path: "/paymentcallback/:method",
  //   name: 'PaymentCallback',
  //   is_regx: true,
  //   component: PaymentCallback
  // }
];

export default routes;