export const OAUTH2_TOKENS_KEY = 'oauth2_tokens';
export const API_ROOT_PATH = `${window.zento.server}/api/v1`;
export const IMAGE_SERVER = window.zento.image_server;
export const API_ENDPOINTS = {
    AppConfigs: "/reactapp/configs",
    oauth2: {
        token: '/oauth2/token',
        refresh: '/oauth2/refresh',
        register: '/oauth2/register',
        logout: '/oauth2/logout'
    },
    customer: {
        me: '/customers/me',
        guest: '/customers/guest',
        address: '/customers/me/addresses'
    },
    cart: {
        mime: "/cart/mine",
        items: "/cart/mine/items",
        putEmail: "/cart/mine/email",
    },
    urlfinder: "/urlrewrite"
}

export function genApiUrl(path) {
    return `${API_ROOT_PATH}/${path}`;
}

export function genProductImageUrl(path) {
    return `${IMAGE_SERVER}/product/${path}`;
}

export function genCategoryImageUrl(path) {
    return `${IMAGE_SERVER}/category/${path}`;
}
