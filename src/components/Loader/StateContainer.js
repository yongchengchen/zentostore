// @flow

import {
    Container
} from "unstated";

type StateContainerModel = {
    show: number,
    tips ? : {},
    error ? : boolean,
    error_msg ? : string,
    on_click ? : any
}

export class StateContainer extends Container < StateContainerModel > {
    state = {
        show: 0,
        tips: {},
        error: false,
        error_msg: "",
        on_click: () => {}
    };
    _timer: any;
    _interval: any;

    _queue: Array < StateContainerModel > = [];
    _tips: {} = {};
    _showing_ids = {};

    _idx: number = 0;


    _startInterval() {
        if (!this._interval) {
            this._interval = setInterval(() => {
                if (this._queue.length > 0) {
                    let item = this._queue.shift();
                    let newState: StateContainerModel = {
                        show: this.state.show + item.show,
                        error: item.error,
                        error_msg: item.error_msg
                    }
                    if (typeof item.on_click === 'function') {
                        newState["on_click"] = item.on_click
                    } else {
                        if (item.on_click === 'unmount') {
                            newState["on_click"] = () => {}
                        }
                    }
                    this.setState(newState);
                } else {
                    this._startTimer();
                }
            }, 100);
        }
    }

    _startTimer() {
        if (!this._timer) {
            this._timer = setTimeout(() => {
                if (this._queue.length === 0 && this.state.show === 0) {
                    if (this._interval) {
                        clearInterval(this._interval);
                        this._interval = null;
                    }
                    this._timer = null;
                    this._tips = {};
                }
            }, 1000);
        }
    }

    show(tip: string, on_click ? : any) {
        this._idx++;
        if (tip && tip !== "") {
            this._tips[tip] = this._tips[tip] || [];
            this._tips[tip].push(this._idx);
            this._showing_ids[this._idx] = tip;
        }
        this._queue.push({
            show: 1,
            on_click: on_click
        });
        this._startInterval();
        return this._idx;
    }

    error(tip: string) {
        let idxArr = [1, 2, 3, 4, 5];
        idxArr.forEach(item => {
            this._queue.push({
                show: 1,
                error: true,
                error_msg: tip
            });
        });

        idxArr.forEach(item => {
            this._queue.push({
                show: -1
            });
        });
    }

    hide(showHandler: number, on_click ? : any) {
        if (showHandler) {
            let tip = this._showing_ids[showHandler];
            if (tip) {
                let ids = this._tips[tip];
                var index = ids.indexOf(showHandler);
                if (index > -1) {
                    ids.splice(index, 1);
                }
                if (ids.length === 0) {
                    delete this._tips[tip];
                } else {
                    this._tips[tip] = ids;
                }
            }
            delete this._showing_ids[showHandler];
        }
        this._queue.push({
            show: -1,
            on_click: on_click
        });
    }

    getTips() {
        return this._tips;
    }
}
