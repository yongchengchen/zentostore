import React, { Component } from "react";
import { Provider, Subscribe } from "unstated";
import className from "classnames";
import { StateContainer } from "./StateContainer";
import CircularProgress from "@material-ui/core/CircularProgress";

import "./style.css";

const container = new StateContainer();

class Layer extends Component {
  onClick = e => {
    if (typeof container.state.on_click === "function") {
      container.state.on_click.call();
    }
  };
  render() {
    return (
      <Provider>
        <Subscribe to={[container]}>
          {container => (
            <div
              className={className("loading-mask", {
                "modal-hidden": container.state.show <= 0
              })}
              onClick={this.onClick}
            >
              <div className="loader">
                <CircularProgress
                  className={"loadercircle"}
                  color="secondary"
                />
                <div className="text-container">
                  <div className="loader-tip">
                    {Object.keys(container.getTips()).map((tip, idx) => {
                      return (
                        <div
                          key={idx}
                          dangerouslySetInnerHTML={{ __html: tip }}
                        />
                      );
                    })}
                  </div>
                  <div className="loader-error">
                    {container.state.error_msg}
                  </div>
                </div>
              </div>
            </div>
          )}
        </Subscribe>
      </Provider>
    );
  }
}

const show = (tip, on_click) => {
  return container.show(tip, on_click);
};

const error = tip => {
  container.error(tip);
};

const hide = (showHandler, on_click) => {
  container.hide(showHandler, on_click);
};

const Loader = {
  Layer: Layer,
  show: show,
  error: error,
  hide: hide
};
export default Loader;
