import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Provider, Subscribe } from "unstated";

import { ShoppingCartAgent } from "../models";
import { genProductImageUrl } from "../consts";

class MiniCart extends Component {
  deleteCartItem = id => e => {
    ShoppingCartAgent.deleteCartItem(id);
  };

  render() {
    return (
      <Provider>
        <Subscribe to={[ShoppingCartAgent]}>
          {container => {
            return (
              <div className="cart-dropdown" onClick={this.toggleCart}>
                {ShoppingCartAgent.state.items_quantity ? (
                  <React.Fragment>
                    <div className="cart-list">
                      {ShoppingCartAgent.state.items.map((item, idx) => {
                        return (
                          <div key={idx} className="product-widget">
                            <div className="product-img">
                              <img
                                src={genProductImageUrl(item.image)}
                                alt="Product"
                                width="120px"
                              />
                            </div>
                            <div className="product-body">
                              <h3 className="product-name">
                                <a href="/">{item.name}</a>
                              </h3>
                              <h4 className="product-price">
                                <span className="qty">{item.quantity}x</span>$
                                {item.price}
                              </h4>
                            </div>
                            <button
                              className="delete"
                              onClick={this.deleteCartItem(item.id)}
                            >
                              <i className="fa fa-close" />
                            </button>
                          </div>
                        );
                      })}
                    </div>
                    <div className="cart-summary">
                      <small>
                        {ShoppingCartAgent.state.items_quantity} Item(s)
                        selected
                      </small>
                      <h5>SUBTOTAL: ${ShoppingCartAgent.state.subtotal}</h5>
                    </div>
                    <div className="cart-btns">
                      <NavLink to="/cart">View Cart</NavLink>
                      <NavLink to="/checkout">
                        Checkout <i className="fa fa-arrow-circle-right" />
                      </NavLink>
                    </div>
                  </React.Fragment>
                ) : (
                  <div className="cart-summary">Your Cart Is Empty</div>
                )}
              </div>
            );
          }}
        </Subscribe>
      </Provider>
    );
  }
}
export default MiniCart;
