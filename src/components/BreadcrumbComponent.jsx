import React, { Component } from "react";
import Breadcrumbs from "@material-ui/lab/Breadcrumbs";
import { NavLink } from "react-router-dom";
import { CatalogAgent } from "../models";

class BreadcrumbComponent extends Component {
  buildItems = () => {
    let items = [{ title: "Home", to: "/" }];
    const { category_id, product, extra } = this.props;
    if (category_id) {
      let category = CatalogAgent.getCategoryById(category_id);
      if (category) {
        let parentIds = category.path.split("/");
        for (let i = 1; i < parentIds.length; i++) {
          let tmpCat = CatalogAgent.getCategoryById(parentIds[i]);
          if (tmpCat) {
            items.push({
              title: tmpCat.name,
              to: "/" + tmpCat.url_path
            });
          }
        }
      }
    }
    if (product) {
      items.push({
        title: product.name,
        to: "/" + product.url_key
      });
    }

    if (extra) {
      items.push(extra);
    }
    return items;
  };

  render() {
    const items = this.buildItems();
    return (
      <Breadcrumbs arial-label="Breadcrumb">
        {items.map((item, idx) => {
          return item.to ? (
            <NavLink key={idx} color="inherit" to={item.to}>
              {item.title}
            </NavLink>
          ) : (
            <span key={idx}>{item.title}</span>
          );
        })}
      </Breadcrumbs>
    );
  }
}
export default BreadcrumbComponent;
