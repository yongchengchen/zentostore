import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import LayoutBody from "./LayoutBody";

import { styles } from "../theme/sectionlayout";

function SectionLayout(props) {
  const { children, classes, backgroundImage, withbackdrop } = props;

  return (
    <section className={classes.root}>
      <LayoutBody className={classes.layoutBody} width="full">
        {children}
        {withbackdrop ? <div className={classes.backdrop} /> : ""}
        <div
          className={classNames(classes.background)}
          style={{
            backgroundImage: `url(${backgroundImage})`
          }}
        />
      </LayoutBody>
    </section>
  );
}

SectionLayout.propTypes = {
  backgroundImage: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SectionLayout);
