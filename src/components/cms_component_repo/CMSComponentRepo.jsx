import React from "react";

/**
 * so backend we can configurate to use these dynamic components
 */
class ComponentRepoClass {
  components = {};
  register(name, component) {
    this.components[name] = component;
  }

  getComponent(name) {
    if (typeof name === "string") {
      return this.components[name];
    }
    if (typeof name === "object") {
      console.log("getComponent", name);
      return name;
    }
    return name;
  }

  has(name) {
    return this.components[name];
  }
}

let CMSComponentRepo = new ComponentRepoClass();

const Carousel = React.lazy(() => import("./section/Carousel"));
const ImageSection = React.lazy(() => import("./section/ImageSection"));
const ProductCarousel = React.lazy(() => import("./section/ProductCarousel"));
const Gallery = React.lazy(() => import("./section/Gallery"));

CMSComponentRepo.register("Carousel", Carousel);
CMSComponentRepo.register("ImageSection", ImageSection);
CMSComponentRepo.register("ProductCarousel", ProductCarousel);
CMSComponentRepo.register("Gallery", Gallery);
export default CMSComponentRepo;
