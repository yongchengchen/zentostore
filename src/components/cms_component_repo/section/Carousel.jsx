import React from "react";
import PropTypes from "prop-types";
import ImageSection from "./ImageSection";
import Slider from "react-slick";

const settings = {
  dots: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  speed: 500,
  autoplaySpeed: 3000,
  cssEase: "linear",
  pauseOnHover: true
};

function Carousel(props) {
  const { data } = props;

  return (
    <Slider {...settings}>
      {data.map((item, idx) => {
        return <ImageSection key={idx} data={item} />;
      })}
    </Slider>
  );
}

Carousel.propTypes = {
  data: PropTypes.array.isRequired
};

export default Carousel;
