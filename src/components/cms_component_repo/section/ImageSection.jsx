import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { NavLink } from "react-router-dom";
import Button from "../../Button";
import Typography from "../../Typography";
import SectionLayout from "../../SectionLayout";

import { styles } from "../../../theme/imagesection";

function ImageSection(props) {
  const { classes, data } = props;
  const toHref = data.to || data.href || data.url_key || "/";
  return (
    <SectionLayout backgroundImage={data.image} withbackdrop={true}>
      <Typography
        color="inherit"
        align="center"
        variant="h2"
        className={classes.title}
        marked="center"
      >
        {data.title}
      </Typography>
      <Typography
        color="inherit"
        align="center"
        className={classes.description}
      >
        {data.description ? data.description : ""}
      </Typography>
      <Button
        color="secondary"
        variant="contained"
        size="large"
        className={classes.button}
        component={linkProps => (
          <NavLink {...linkProps} to={toHref} variant="button" />
        )}
      >
        View
      </Button>
      <Typography variant="body2" color="inherit" className={classes.more} />
    </SectionLayout>
  );
}

ImageSection.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ImageSection);
