import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

import { withStyles } from "@material-ui/core/styles";
import LayoutBody from "../../LayoutBody";
import Typography from "../../Typography";

import { styles } from "../../../theme/promotionsection";

function Gallery(props) {
  const { classes, data } = props;
  return (
    <LayoutBody className={classes.root} component="section" width="full">
      <div className={classes.images}>
        {data.map((item, idx) => (
          <NavLink
            to={item.href}
            key={idx}
            className={classes.imageWrapper}
            style={{
              width: item.width
            }}
          >
            <div
              className={classes.imageSrc}
              style={{
                backgroundImage: `url(${item.image})`
              }}
            />
            <div className={classes.imageBackdrop} />
            <div className={classes.imageButton}>
              <Typography
                component="h3"
                variant="h6"
                color="inherit"
                className={classes.imageTitle}
              >
                {item.title}
                <div className={classes.imageMarked} />
              </Typography>
            </div>
          </NavLink>
        ))}
      </div>
    </LayoutBody>
  );
}

Gallery.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Gallery);
