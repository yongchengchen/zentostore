import React from "react";
import PropTypes from "prop-types";
import Slider from "react-slick";
import Typography from "../../Typography";
import ProductCard from "../../product/ProductCard";
import { deviceSizeType } from "../../../support/helper";

function getSliderSettings() {
  let settings = {
    dots: true,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    speed: 500,
    autoplaySpeed: 3000,
    cssEase: "linear",
    pauseOnHover: true
  };
  if (deviceSizeType(["md"])) {
    settings.slidesToShow = 3;
    settings.slidesToScroll = 3;
  } else {
    if (deviceSizeType(["sm"])) {
      settings.slidesToShow = 2;
      settings.slidesToScroll = 2;
    } else {
      if (deviceSizeType(["xs"])) {
        settings.slidesToShow = 1;
        settings.slidesToScroll = 1;
      }
    }
  }
  return settings;
}

function ProductCarousel(props) {
  const { data } = props;
  return (
    <section>
      <Typography color="inherit" align="center" variant="h2" marked="center">
        {data.title}
      </Typography>
      <Slider {...getSliderSettings()}>
        {data.items.map((product, idx) => {
          return <ProductCard key={idx} product={product} />;
        })}
      </Slider>
    </section>
  );
}

ProductCarousel.propTypes = {
  data: PropTypes.object.isRequired
};

export default ProductCarousel;
