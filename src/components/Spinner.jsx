import React, { Component } from "react";

import className from "classnames";
import CircularProgress from "@material-ui/core/CircularProgress";

class Spinner extends Component {
  render() {
    const { show } = this.props;
    return (
      <div
        className={className("pagedata-loader-container", {
          "hidden-element": !show
        })}
      >
        <CircularProgress className={"pagedata-loader"} color="secondary" />
      </div>
    );
  }
}

export default Spinner;
