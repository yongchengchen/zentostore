import React from "react";

class BaseSwatchComponent extends React.Component {
  swatchOptionHandler = (state, optionItem) => {};

  onChangeSwatchOption = (swatch_type, swatch_key) => e => {
    const { options, swatch_items } = this.props;
    const optionItem = swatch_items[swatch_type][swatch_key];

    let state = options.value();
    this.swatchOptionHandler(state, optionItem);
    state.options[swatch_type] = swatch_key;
    state.swatch_items = swatch_items[swatch_type];
    options.mergeValue(state);
    console.log("tony swatch", swatch_type, swatch_key, options.state.value);
  };

  render() {
    return "";
  }
}
export default BaseSwatchComponent;
