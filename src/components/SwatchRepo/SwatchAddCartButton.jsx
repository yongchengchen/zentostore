import React from "react";
import Button from "@material-ui/core/Button";

import { ShoppingCartAgent, CatalogAgent } from "../../models";
import SwatchRepo from "./index";
import Loader from "../Loader";

class SwatchAddCartButton extends React.Component {
  addToCart = e => {
    const { product } = this.props;
    if (product.has_options) {
      return this.addToCartWithSwatch();
    }
    ShoppingCartAgent.addProduct(product.id, this.qty);
  };

  addToCartWithSwatch = e => {
    const { swatch_items, product, fix_qty } = this.props;
    const { options } = SwatchRepo.getSwatchOptionsState(product).value();
    const optionKeys = Object.keys(options);
    if (Object.keys(swatch_items).length !== optionKeys.length) {
      Loader.error("Please select options first");
    } else {
      let products = CatalogAgent.findSubProductByOptions(product, options);
      let qty = fix_qty
        ? fix_qty
        : CatalogAgent.getProduct(product.id).getTempData("quantity");
      ShoppingCartAgent.addProduct(product.id, qty ? qty : 1, {
        item_id: products[0]["id"],
        swatches: options
      });
    }
  };

  render() {
    return (
      <Button variant="contained" color="primary" onClick={this.addToCart}>
        <i className="fa fa-shopping-cart" /> add to cart
      </Button>
    );
  }
}
export default SwatchAddCartButton;
