import React from "react";
import { withStyles } from "@material-ui/core/styles";
import className from "classnames";

import BaseSwatchComponent from "./BaseSwatchComponent";

import { styles } from "../../theme/swatch";

const SWATCH_TYPE = "color";

class SwatchColor extends BaseSwatchComponent {
  swatchOptionHandler = (state, optionItem) => {
    state.main_image = optionItem.image;
    state.media_gallery =
      optionItem.products.length > 0
        ? optionItem.products[0].media_gallery
        : null;
  };

  render() {
    const { options, swatch_items, classes } = this.props;
    if (!swatch_items[SWATCH_TYPE]) {
      return "";
    }

    return (
      <div className={className(classes.optionsContainer, "clearfix")}>
        {Object.keys(swatch_items[SWATCH_TYPE]).map((color, idx) => {
          const optionItem = swatch_items[SWATCH_TYPE][color];
          const selectOptions = options.value().options;

          const optionClasses = {};
          optionClasses[classes.enabled] = optionItem.available > 0;
          optionClasses[classes.disabled] = optionItem.available <= 0;
          optionClasses[classes.selected] = selectOptions.color === color;

          return (
            <div
              key={idx}
              className={className(
                classes.option,
                classes.color,
                optionClasses
              )}
              style={{
                background: optionItem.swatch_value
              }}
              onClick={this.onChangeSwatchOption(SWATCH_TYPE, color)}
            />
          );
        })}
      </div>
    );
  }
}
export default withStyles(styles)(SwatchColor);
