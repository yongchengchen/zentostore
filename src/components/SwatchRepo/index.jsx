import SwatchColor from "./Color";
import SwatchSize from "./Size";
import { AppConfigsContainer, ContainerProvider } from "../../models";

class SwatchRepoClass {
  getComponent(option) {
    switch (option) {
      case "color":
        return SwatchColor;
      case "size":
        return SwatchSize;
      default:
        break;
    }
  }

  getSwatchOptionsState(product, defaultOption) {
    defaultOption = defaultOption
      ? defaultOption
      : {
          media_gallery: product.media_gallery,
          main_image: product.image,
          options: {},
          swatch_items: {}
        };
    return ContainerProvider.state(`prod.${product.id}.options`, defaultOption);
  }

  initSwatchOptions(product) {
    if (product && product.configurables) {
      let configs = AppConfigsContainer.state;
      let swatchOptions = {};
      let types = Object.keys(configs.swatches ? configs.swatches : {});
      types.forEach(key => {
        let options = configs.swatches[key];
        let items = {};
        product.configurables.forEach(configurable => {
          let attr = configurable[key];
          if (attr) {
            if (!items[attr]) {
              items[attr] = {
                parent_id: product.id,
                swatch_value: options[attr],
                active: true,
                image: configurable.image,
                products: [configurable],
                available: 1
              };
            } else {
              items[attr]["products"].push(configurable);
              items[attr]["available"] = items[attr]["products"].length;
            }
          }
        });
        swatchOptions[key] = items;
      });
      this._filterSwatch(swatchOptions, product);
      return swatchOptions;
    }
    return false;
  }

  /**
   *
   * @param {*} swatchOptions  {color:Red, size:10}
   * @param {*} selectedSwatches
   */
  _filterSwatch(swatchOptions, parentProduct) {
    let swatches = this.getSwatchOptionsState(parentProduct).value().options;
    let types = Object.keys(swatchOptions);
    Object.keys(swatches).forEach(selectedName => {
      types.forEach(swatchName => {
        if (selectedName !== swatchName) {
          let items = swatchOptions[swatchName];
          Object.keys(items).forEach(key => {
            let config = items[key];
            if (config.available > 0) {
              config.products.forEach(product => {
                if (product[selectedName] !== swatches[selectedName]) {
                  config.available = config.available - 1;
                }
              });
            }
          });
        }
      });
    });
  }
}

let SwatchRepo = new SwatchRepoClass();
export default SwatchRepo;
