import React from "react";
import { ContainerProvider } from "../../models";
import { withStyles } from "@material-ui/core/styles";
import className from "classnames";

import BaseSwatchComponent from "./BaseSwatchComponent";

import { styles } from "../../theme/swatch";

const SWATCH_TYPE = "size";
class SwatchSize extends BaseSwatchComponent {
  render() {
    const { swatch_items, classes } = this.props;
    if (!swatch_items[SWATCH_TYPE]) {
      return "";
    }
    return (
      <div className={className(classes.optionsContainer, "clearfix")}>
        {Object.keys(swatch_items[SWATCH_TYPE]).map((size, idx) => {
          let optionItem = swatch_items[SWATCH_TYPE][size];
          const stateKey = `prod.${optionItem.parent_id}.options`;
          const selectOptions = ContainerProvider.state(stateKey).value()
            .options;

          const optionClasses = {};
          optionClasses[classes.enabled] = optionItem.available > 0;
          optionClasses[classes.disabled] = optionItem.available <= 0;
          optionClasses[classes.selected] = selectOptions.size === size;
          return (
            <div
              key={idx}
              className={className(classes.option, optionClasses)}
              data-filter={SWATCH_TYPE}
              data-value={size}
              onClick={this.onChangeSwatchOption(SWATCH_TYPE, size)}
            >
              {optionItem.swatch_value}
            </div>
          );
        })}
      </div>
    );
  }
}
export default withStyles(styles)(SwatchSize);
