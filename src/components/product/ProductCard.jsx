import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import ProductCardWithSwatch from "./WithSwatch";
import ProductCardWithoutSwatch from "./WithoutSwatch";
import SwatchRepo from "../SwatchRepo";

import { styles } from "../../theme/productcard";

class ProductCard extends Component {
  render() {
    const swatchItems = SwatchRepo.initSwatchOptions(this.props.product);
    return swatchItems ? (
      <ProductCardWithSwatch swatch_items={swatchItems} {...this.props} />
    ) : (
      <ProductCardWithoutSwatch {...this.props} />
    );
  }
}

ProductCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ProductCard);
