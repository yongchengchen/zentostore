import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";

import { ShoppingCartAgent } from "../../models";
import { genProductImageUrl } from "../../consts";

class ProductCardWithoutSwatch extends Component {
  addToCart = e => {
    const { product } = this.props;
    ShoppingCartAgent.addProduct(product.id, 1);
  };

  render() {
    const { classes, product } = this.props;
    return (
      <Card className={classes.card}>
        <NavLink
          className="quick-view"
          to={"/" + this.props.product.url_key + ".html"}
        >
          <CardMedia
            className={classes.media}
            image={genProductImageUrl(product.image)}
            title={product.name}
          />
        </NavLink>
        <CardContent className={classes.swatchContainer}>
          <Typography component="p">{product.name}</Typography>
          <Typography component="p">${product.price}</Typography>
        </CardContent>
        <CardActions className={classes.actions}>
          <Button
            variant="contained"
            color="primary"
            aria-label="Add to cart"
            onClick={this.addToCart}
          >
            Add to Cart
          </Button>
          <IconButton
            aria-label="Add to favorites"
            className={classes.likeIcon}
          >
            <FavoriteIcon />
          </IconButton>
        </CardActions>
      </Card>
    );
  }
}

ProductCardWithoutSwatch.propTypes = {
  classes: PropTypes.object.isRequired
};

export default ProductCardWithoutSwatch;
