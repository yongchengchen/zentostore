import React, { Component } from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import {
  CatalogAgent,
  ShoppingCartAgent,
  PageSettingCenter
} from "../../models";
import { Provider, Subscribe } from "unstated";

import ProductCard from "./ProductCard";

class ProductList extends Component {
  addToCart = (productId, qty) => e => {
    ShoppingCartAgent.addProduct(productId, qty);
  };

  onPerPageChanged = e => {
    const per_page = Number(e.target.value);
    PageSettingCenter.setPerPage(per_page);
    CatalogAgent.appendCriteraFilter({ per_page: per_page });
  };

  onSortByChanged = e => {
    const sort_by = e.target.value;
    PageSettingCenter.setSortBy(sort_by);
    CatalogAgent.appendCriteraFilter({ sort_by: sort_by });
  };

  render() {
    const { paginator } = this.props;
    return (
      <Grid container>
        <Grid item xs={12}>
          <Provider>
            <Subscribe to={[PageSettingCenter]}>
              {settings => (
                <div className="store-sort">
                  <label>
                    Sort By:
                    <select
                      className="input-select"
                      value={settings.state.sort_by}
                      onChange={this.onSortByChanged}
                    >
                      {settings.state.sort_bys.map((item, idx) => {
                        return (
                          <option key={idx} value={item.code}>
                            {item.title}
                          </option>
                        );
                      })}
                    </select>
                  </label>
                  <label>
                    Show:
                    <select
                      className="input-select"
                      value={settings.state.per_page}
                      onChange={this.onPerPageChanged}
                    >
                      {settings.state.per_pages.map((value, idx) => {
                        return (
                          <option key={idx} value={value}>
                            {value}
                          </option>
                        );
                      })}
                    </select>
                  </label>
                  <label>
                    <span>
                      Items {paginator.from}-{paginator.to} of {paginator.total}
                    </span>
                  </label>
                </div>
              )}
            </Subscribe>
          </Provider>
        </Grid>
        <Grid item xs={12}>
          <Grid container>
            {paginator.data.map(function(product, idx) {
              return (
                <Grid item xs={12} sm={6} md={4}>
                  <ProductCard key={idx} product={product} />
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

ProductList.propTypes = {
  paginator: PropTypes.object.isRequired
};

export default ProductList;
