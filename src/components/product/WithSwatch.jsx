import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Provider, Subscribe } from "unstated";
import PropTypes from "prop-types";

import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";

import SwatchAddCartButton from "../SwatchRepo/SwatchAddCartButton";
import { AppConfigsContainer } from "../../models";
import { genProductImageUrl } from "../../consts";

import SwatchRepo from "../SwatchRepo";

class ProductCardWithSwatch extends Component {
  render() {
    const { swatch_items, classes, product } = this.props;
    const optionsState = SwatchRepo.getSwatchOptionsState(product);
    return (
      <Card className={classes.card}>
        <NavLink className="quick-view" to={"/" + this.props.product.url_key}>
          <Provider>
            <Subscribe to={[optionsState]}>
              {container => {
                return (
                  <CardMedia
                    className={classes.media}
                    image={genProductImageUrl(container.value().main_image)}
                    title={product.name}
                  />
                );
              }}
            </Subscribe>
          </Provider>
        </NavLink>
        <CardContent className={classes.swatchContainer}>
          <p>{product.name}</p>
          <p>${product.price}</p>
          <Provider>
            <Subscribe to={[AppConfigsContainer, optionsState]}>
              {stateContainer => {
                return Object.keys(
                  AppConfigsContainer.state.swatches
                    ? AppConfigsContainer.state.swatches
                    : {}
                ).map((option, idx) => {
                  const SwatchComponent = SwatchRepo.getComponent(option);
                  return (
                    <SwatchComponent
                      swatch_items={swatch_items}
                      key={idx}
                      parent_id={product.id}
                      options={optionsState}
                    />
                  );
                });
              }}
            </Subscribe>
          </Provider>
        </CardContent>
        <CardActions className={classes.actions}>
          <SwatchAddCartButton
            swatch_items={swatch_items}
            product={product}
            fix_qty={1}
          />
          <IconButton
            aria-label="Add to favorites"
            className={classes.likeIcon}
          >
            <FavoriteIcon />
          </IconButton>
        </CardActions>
      </Card>
    );
  }
}

ProductCardWithSwatch.propTypes = {
  classes: PropTypes.object.isRequired
};

export default ProductCardWithSwatch;
