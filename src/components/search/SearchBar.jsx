import React from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import { withStyles } from "@material-ui/core/styles";

import { styles } from "../../theme/searchbar";

class SearchBar extends React.Component {
  state = { text: "" };

  inputValue = e => {
    this.setState({ text: e.target.value });
  };

  handleKeyDown = e => {
    if (e.key === "Enter") {
      this.search();
    }
  };

  search = () => {
    const { text } = this.state;
    if (text && text !== "") {
      const { onSearch, history } = this.props;
      history.push(`/search?text=${text}`);
      if (onSearch) {
        onSearch();
      }
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.searchBar}>
        <div className={classes.searchIcon}>
          <IconButton color="inherit" onClick={this.search}>
            <SearchIcon />
          </IconButton>
        </div>
        <InputBase
          placeholder="Search…"
          classes={{
            input: classes.searchInput
          }}
          value={this.state.text}
          onChange={this.inputValue}
          onKeyDown={this.handleKeyDown}
        />
      </div>
    );
  }
}

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(SearchBar));
