import React, { Component } from "react";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import Switch from "@material-ui/core/Switch";

import { deviceSizeType } from "../../support/helper";
import { CatalogAgent } from "../../models";

class DynamicAttributeFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
  }

  onDynAttrItemChange = id => e => {
    const { attr_name } = this.props;
    const attrs = CatalogAgent.getFilter(attr_name) || [];

    let idx = attrs.indexOf(id);
    if (idx > -1) {
      attrs.splice(idx, 1);
    } else {
      attrs.push(id);
    }
    if (deviceSizeType(["xs"])) {
      this.onExpanded(null, false);
    }
    const filters = {};
    filters[attr_name] = attrs;
    CatalogAgent.appendCriteraFilter(filters);
  };

  onExpanded = (e, flg) => {
    this.setState({ expanded: flg });
  };

  render() {
    const { attr_name, data } = this.props;
    const attrs = CatalogAgent.getFilter(attr_name) || [];

    let isOn = false;
    return (
      <div className="row">
        <ExpansionPanel
          expanded={this.state.expanded}
          onChange={this.onExpanded}
          className={data.length ? "" : "hidden-element"}
        >
          <ExpansionPanelDetails>
            <FormControl component="fieldset">
              <FormGroup>
                {data.map((item, idx) => {
                  let checked = attrs.includes(item.key);
                  isOn = isOn || checked;
                  return (
                    <label key={idx}>
                      <Switch
                        checked={checked}
                        onChange={this.onDynAttrItemChange(item.key)}
                      />
                      <span
                        key={idx}
                        className="nav-link"
                        dangerouslySetInnerHTML={{
                          __html: item.key + "(" + item.doc_count + ")"
                        }}
                      />
                    </label>
                  );
                })}
              </FormGroup>
            </FormControl>
          </ExpansionPanelDetails>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <div>
              <span style={{ color: isOn ? "red" : "black" }}>
                {this.props.label}
              </span>
            </div>
          </ExpansionPanelSummary>
        </ExpansionPanel>
      </div>
    );
  }
}
export default DynamicAttributeFilter;
