import React, { Component } from "react";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import Switch from "@material-ui/core/Switch";

import { CatalogAgent } from "../../models";
import { RouteContainer } from "../../dynamicroute";

import { deviceSizeType } from "../../support/helper";

const FILTER_NAME = "sub_categories";
class CategoryFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
  }

  onCategoryItemChange = id => e => {
    const sub_categories = CatalogAgent.getFilter(FILTER_NAME) || [];
    let idx = sub_categories.indexOf(id);
    if (idx < 0) {
      idx = sub_categories.indexOf(String(id));
    }
    if (idx > -1) {
      sub_categories.splice(idx, 1);
    } else {
      sub_categories.push(id);
    }

    if (deviceSizeType(["xs"])) {
      this.onExpanded(null, false);
    }
    const filters = {};
    filters[FILTER_NAME] = sub_categories;
    CatalogAgent.appendCriteraFilter(filters);
  };

  onExpanded = (e, flg) => {
    this.setState({ expanded: flg });
  };

  rebuildCategoryTree = items => {
    items = items || {};
    let categories = {};
    for (let item of items) {
      categories[item.id] = CatalogAgent.getCategoryById(item.id);
    }
    return categories;
  };

  render() {
    const { data } = this.props;
    const sub_categories = CatalogAgent.getFilter(FILTER_NAME) || [];
    const categoryId = RouteContainer.value().params.id;
    let items = data.filter(item => {
      return categoryId != item.key;
    });
    const categories = this.rebuildCategoryTree(items);

    let isOn = false;
    const keys = Object.keys(items);
    return (
      <ExpansionPanel
        expanded={this.state.expanded}
        onChange={this.onExpanded}
        className={keys.length ? "" : "hidden-element"}
      >
        <ExpansionPanelDetails>
          <FormControl component="fieldset">
            <FormGroup>
              {items.map((item, idx) => {
                const category = categories[item.key] || item;
                let checked =
                  sub_categories.includes(item.key) ||
                  sub_categories.includes(String(item.key));
                isOn = isOn || checked;
                return (
                  <label key={idx}>
                    <Switch
                      checked={checked}
                      onChange={this.onCategoryItemChange(item.key)}
                    />
                    <span
                      key={idx}
                      className="nav-link"
                      dangerouslySetInnerHTML={{
                        __html: category.name + "(" + item.doc_count + ")"
                      }}
                    />
                  </label>
                );
              })}
            </FormGroup>
          </FormControl>
        </ExpansionPanelDetails>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <div>
            <span style={{ color: isOn ? "red" : "black" }}>
              {this.props.label}
            </span>
          </div>
        </ExpansionPanelSummary>
      </ExpansionPanel>
    );
  }
}
export default CategoryFilter;
