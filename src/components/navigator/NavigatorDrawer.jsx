import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ClearIcon from "@material-ui/icons/Clear";
import IconButton from "@material-ui/core/IconButton";

import { withRouter } from "react-router-dom";

import { Provider, Subscribe } from "unstated";
import { CatalogAgent } from "../../models";
import { SearchBar } from "../../components";

import { drawerWidth, styles } from "../../theme/navigatordrawer";

class NavigatorDrawer extends React.Component {
  state = { text: "" };

  close = () => {
    const { onClose } = this.props;
    onClose();
  };

  clickLink = url => e => {
    this.props.history.push(`/${url}`);
    this.close();
  };

  render() {
    const { classes, open, onClose, variant } = this.props;
    return (
      <Drawer
        variant={variant}
        open={open}
        onClose={onClose}
        classes={{
          paper: classes.drawerPaper
        }}
        PaperProps={{ style: { width: drawerWidth, top: "1px" } }}
      >
        <List disablePadding>
          <ListItem>
            <IconButton
              color="inherit"
              className={classes.closeButton}
              onClick={this.close}
            >
              <ClearIcon />
            </IconButton>
          </ListItem>
          <ListItem>
            <SearchBar onSearch={onClose} />
          </ListItem>

          <Provider>
            <Subscribe to={[CatalogAgent.getCategoriesContainer()]}>
              {container =>
                container.state.items.map(({ id, name, children }) => (
                  <React.Fragment key={id}>
                    <ListItem className={classes.categoryHeader}>
                      <ListItemText
                        classes={{
                          primary: classes.categoryHeaderPrimary
                        }}
                      >
                        {name}
                      </ListItemText>
                    </ListItem>
                    {children.map(
                      ({
                        id,
                        name,
                        is_active,
                        url_path,
                        children: subchildren
                      }) => (
                        <React.Fragment key={id}>
                          <ListItem
                            button
                            dense
                            key={id}
                            className={classNames(
                              classes.item,
                              is_active && classes.itemActiveItem
                            )}
                            onClick={this.clickLink(url_path)}
                          >
                            <ListItemText
                              classes={{
                                primary: classes.itemPrimary,
                                textDense: classes.textDense
                              }}
                            >
                              {name}
                            </ListItemText>
                          </ListItem>
                          {subchildren.map(
                            ({ id, name, url_path, is_active }) => (
                              <ListItem
                                button
                                dense
                                key={id}
                                onClick={this.clickLink(url_path)}
                                className={classNames(
                                  classes.item,
                                  classes.subitem,
                                  is_active && classes.itemActiveItem
                                )}
                              >
                                {/* <ListItemIcon>{icon}</ListItemIcon> */}
                                <ListItemText
                                  classes={{
                                    primary: classes.itemPrimary,
                                    textDense: classes.textDense
                                  }}
                                >
                                  {name}
                                </ListItemText>
                              </ListItem>
                            )
                          )}
                        </React.Fragment>
                      )
                    )}
                    <Divider className={classes.divider} />
                  </React.Fragment>
                ))
              }
            </Subscribe>
          </Provider>
        </List>
      </Drawer>
    );
  }
}

NavigatorDrawer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(NavigatorDrawer));
