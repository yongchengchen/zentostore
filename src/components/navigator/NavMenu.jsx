import React, { Component } from "react";

import NavMenuItem from "./NavMenuItem";

class NavMenu extends Component {
  render() {
    let { items } = this.props;
    return (
      <ul className="navbar-nav nav">
        {items.map((item, idx) => {
          return <NavMenuItem key={idx} item={item} level={0} />;
        })}
      </ul>
    );
  }
}
export default NavMenu;
