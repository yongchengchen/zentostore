import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import className from "classnames";

class NavMenuItem extends Component {
  render() {
    const item = this.props.item;
    const level = this.props.level || 0;
    const nameWithAmount =
      item.products_count > 0 && level > 0
        ? "(" + item.products_count + ")"
        : "";
    return (
      <li key={item.id} className={className("", { dropdown: level === 0 })}>
        <NavLink
          to={"/" + item.url_path}
          className={className({ "menu-item": level === 0 })}
        >
          {item.name}
          {nameWithAmount}
        </NavLink>
        {item.children && item.children.length > 0 ? (
          <ul className={className("sub-nav", "level-" + level)}>
            {item.children.map((subitem, idx) => {
              return <NavMenuItem key={idx} item={subitem} level={level + 1} />;
            })}
          </ul>
        ) : (
          ""
        )}
      </li>
    );
  }
}
export default NavMenuItem;
