export const styles = theme => ({
    container: {
        width: 500,
        padding: 10,
        overflow: "hidden"
    },

    summaryTip: {
        color: "#000",
        fontWeight: "bold",
        float: "right",
        paddingBottom: 5
    },

    viewCartButton: {
        color: "#fff",
        width: 235,
        backgroundColor: "#d10024",
        '&:hover': {
            backgroundColor: "#d10024",
            opacity: "0.8"
        }
    },

    checkoutButton: {
        color: "#fff",
        width: 235,
        backgroundColor: theme.palette.page.primary,
        '&:hover': {
            backgroundColor: theme.palette.page.primary,
            opacity: "0.8"
        }
    },

    list: {
        maxHeight: 380,
        marginBottom: 15,
        overflowY: "scroll",
        overflowX: "hidden"
    },

    product: {
        padding: 0,
        "-webkit-box-shadow": "none",
    },

    image: {
        left: 0,
        top: 0
    },

    onlyMobile: {
        display: "flex",
        [theme.breakpoints.up("sm")]: {
            display: "none"
        }
    },
    onlyDesktop: {
        display: "none",
        [theme.breakpoints.up("sm")]: {
            display: "flex"
        }
    },

    swatchLable: {
        textTransform: "uppercase",
        paddingRight: 5
    },
    swatchTitle: {
        fontWeight: "bold"
    }
});
