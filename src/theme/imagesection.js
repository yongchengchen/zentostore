export const styles = theme => ({
  title: {
    fontSize: 25,
    [theme.breakpoints.up("sm")]: {
      fontSize: 40
    }
  },

  description: {
    marginBottom: theme.spacing.unit * 4,
    marginTop: theme.spacing.unit * 4,
    [theme.breakpoints.up("sm")]: {
      marginTop: theme.spacing.unit * 6
    }
  }
});