import {
    fade
} from "@material-ui/core/styles/colorManipulator";

export const styles = theme => ({
    searchBar: {
        position: "relative",
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        "&:hover": {
            backgroundColor: fade(theme.palette.common.white, 0.25)
        },
        marginLeft: 0,
        width: "100%",
        [theme.breakpoints.up("md")]: {
            marginLeft: theme.spacing.unit,
            width: "auto",
            display: "flex"
        }
    },
    searchIcon: {
        width: theme.spacing.unit * 5,
        height: "100%",
        position: "absolute",
        right: 0,
        // pointerEvents: "none",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
    },
    searchInput: {
        color: theme.palette.text.light,
        cursor: "pointer",
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit * 5,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 2,
        transition: theme.transitions.create("width"),
        width: "100%",
        [theme.breakpoints.up("sm")]: {
            width: 120,
            "&:focus": {
                width: 200
            }
        }
    }
});
