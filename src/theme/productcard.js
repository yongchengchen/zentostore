export const styles = theme => ({
    card: {
        padding: 5,
        marginLeft: "auto",
        marginRight: "auto",
        "&:hover": {
            overflow: "visible"
        }
    },
    media: {
        maxWidth: 340,
        height: 300,
        display: "block",
        paddingTop: "100%" // 16:9
    },

    swatchContainer: {
        display: "block"
    },

    errorMessage: {
        color: "red"
    },

    actions: {
        display: "inline-block"
    },

    likeIcon: {
        color: "red"
    },
    cartIcon: {
        color: "blue"
    }
});
