import {
    fade
} from "@material-ui/core/styles/colorManipulator";

export const styles = theme => ({
    grow: {
        flexGrow: 1
    },

    menuButton: {
        marginLeft: -12,
        marginRight: 20
    },
    title: {
        display: "none",
        [theme.breakpoints.up("sm")]: {
            display: "block"
        }
    },
    logo: {
        height: 55
    },
});
