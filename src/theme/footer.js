export const styles = theme => ({
    root: {
        display: "flex",
        backgroundColor: theme.palette.page.secondary
    },
    title: {
        color: theme.palette.text.light,
        textTransform: "uppercase",
        fontSize: 18
    },
    layoutBody: {
        // marginLeft: theme.spacing.unit * 4,
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: theme.spacing.unit * 4,
        marginBottom: theme.spacing.unit * 3,
        display: "flex"
    },

    footerContainer: {
        paddingLeft: theme.spacing.unit * 2
    },

    footerLinks: {
        margin: 0,
        paddingLeft: 0,
        listStyle: "none"
    },

    listItem: {
        listStyle: "none",
        paddingTop: theme.spacing.unit / 2,
        paddingBottom: theme.spacing.unit / 2
    },
    link: {
        color: "#fff",
        textDecoration: "none",
        paddingLeft: 5
    },

    copyright: {
        textAlign: "center",
        marginTop: 10,
        display: "block",
        fontSize: 12,
        color: theme.palette.text.light
    }
});
