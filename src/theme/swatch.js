export const styles = theme => ({
  optionsContainer: {
    marginTop: 0
  },

  option: {
    padding: "5px 2px",
    width: "35px",
    height: "30px",
    float: "left",
    margin: "5px 5px 0 0",
    textAlign: "center",
    cursor: "pointer",
    position: "relative",
    border: "1px solid rgb(218, 218, 218)",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },

  enabled: {
    '&:hover': {
      border: "black 2px solid",
      boxShadow: " 2px 2px red"
    }
  },
  selected: {
    border: "black 2px solid",
    boxShadow: " 2px 2px red"
  },

});