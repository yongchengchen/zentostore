import {
    fade
} from "@material-ui/core/styles/colorManipulator";

export const drawerWidth = "95%";

export const styles = theme => ({
    closeButton: {
        color: theme.palette.text.light
    },
    searchInput: {
        paddingLeft: theme.spacing.unit * 1
    },
    drawerPaper: {
        width: drawerWidth,
        top: "100vh",
        backgroundColor: "#9361D6",
        color: "#000"
    },
    categoryHeader: {
        paddingTop: 8,
        paddingBottom: 8
    },
    categoryHeaderPrimary: {
        color: theme.palette.common.white,
        fontSize: "1.3em",
        fontWeight: "bold"
    },
    item: {
        paddingTop: 4,
        paddingBottom: 4
    },
    subitem: {
        marginLeft: 20
    },

    itemActiveItem: {
        color: theme.palette.common.white
    },
    itemPrimary: {
        color: "inherit",
        fontSize: theme.typography.fontSize,
        "&$textDense": {
            fontSize: theme.typography.fontSize
        }
    },
    divider: {
        marginTop: theme.spacing.unit * 2
    }
});
