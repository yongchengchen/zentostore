// @flow

import {
    Container
} from "unstated";

import type {
    Customer
} from "../types";

const GUEST: Customer = {
    id: 0,
    group_id: 0,
    store_id: 0,
    firstname: "guest",
    middlename: "",
    lastname: "",
    email: "",
    guest_email: "",
    is_active: 0,
    is_guest: 1,
    created_from_portal: 0,
    prefix: "",
    suffix: "",
    dob: null,
    default_billing_address_id: 0,
    default_shipping_address_id: 0,
    taxvat: "",
    confirmation: "",
    gender: "",
    failures_num: 0,
    lock_expires: "2099-12-31 23:59:59"
};

export class CustomerContainer extends Container < Customer > {
    state = GUEST;

    setCustomer(customer: Customer) {
        this.setState(customer);
    }

    setGuest() {
        this.setState(GUEST);
    }
}
