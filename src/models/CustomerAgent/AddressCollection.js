// @flow

import {
    Container
} from "unstated";

import type {
    CustomerAddress
} from "../types"


type AddressCollectionState = {
    items: Array < CustomerAddress > ,
    shipping_address ? : CustomerAddress,
    billing_address ? : CustomerAddress
}

export class AddressCollection extends Container < AddressCollectionState > {
    state = {
        items: [],
        shipping_address: null,
        billing_address: null
    };

    getItems() {
        return this.state.items;
    }

    getAmount() {
        const items = this.getItems();
        return items ? items.length : 0;
    }

    getAddressById(address_id: number) {
        if (address_id > 0) {
            const idx = this.state.items.findIndex(item => {
                return address_id === item.id
            })
            if (idx >= 0) {
                return this.state.items[idx];
            }
        }
        return null;
    }

    apply(addresses: Array < CustomerAddress > ) {
        this.setState({
            items: addresses
        });
    }

    setShippingAddress(address: CustomerAddress) {
        let idx = this.state.items.findIndex(item => {
            return address.id === item.id
        })
        if (idx >= 0) {
            this.state.items[idx] = address;
            this.setState({
                shipping_address: address
            });
        }
    }

    setBillingAddress(address: CustomerAddress) {
        const idx = this.state.items.findIndex(item => {
            return address.id === item.id
        })
        if (idx >= 0) {
            this.state.items[idx] = address;
            this.setState({
                billing_address: address
            });
        }
    }

    addAddress(address: CustomerAddress) {
        let idx = this.state.items.findIndex(item => {
            return address.id === item.id
        })
        if (idx < 0) {
            let addresses = this.state.items;
            addresses.push(address);
            this.setState({
                items: addresses
            });
        }
    }

    updateAddress(address: CustomerAddress) {
        let idx = this.state.items.findIndex(item => {
            return address.id === item.id
        })
        if (idx >= 0) {
            let addresses = this.state.items;
            addresses[idx] = address;
            this.setState({
                items: addresses
            });
        }
    }

    deleteAddress(address_id: string) {
        let addresses = this.state.items.filter(item => {
            return address_id !== item.id
        })
        this.setState({
            items: addresses
        });
    }
}
