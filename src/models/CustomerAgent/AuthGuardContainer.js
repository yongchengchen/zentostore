// @flow

import {
    Container
} from "unstated";

import type {
    AuthState
} from "../types"

import {
    OAUTH2_TOKENS_KEY
} from "../../consts";

import HttpClient from "../HttpClient";

export class AuthGuardContainer extends Container < AuthState > {
    state = {
        authed: null,
        login_at: null
    };

    onTokenChanged(oauth2Tokens) {
        localStorage.setItem(OAUTH2_TOKENS_KEY, JSON.stringify(oauth2Tokens));
        HttpClient.reInit();
        this.setState({
            authed: false
        });
    }

    onDeleteToken() {
        localStorage.removeItem(OAUTH2_TOKENS_KEY);
        this.setState({
            authed: null
        });
    }

    onLogin() {
        this.setState({
            authed: true,
            login_at: (new Date())
        });
    }

    onLogout() {
        this.setState({
            authed: false,
            login_at: null
        });
    }

    hasOauthToken() {
        const str = localStorage.getItem(OAUTH2_TOKENS_KEY);
        return str && str !== '';
    }

    getRefreshToken() {
        return localStorage.getItem(OAUTH2_TOKENS_KEY);
    }
}
