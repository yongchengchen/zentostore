// @flow

/**
 * @author yongcheng.chen@live.com
 */
import HttpClient from "../HttpClient";
import GA from "../GA";

import {
    withPrivateProtect
} from "../../support/helper";

import {
    CustomerContainer
} from "./CustomerContainer";

import {
    AddressCollection
} from "./AddressCollection";

import type {
    CustomerAddress
} from "../types";

import {
    AuthGuardContainer
} from "./AuthGuardContainer";

import {
    API_ENDPOINTS
} from "../../consts";

import ShoppingCartAgent from "../ShoppingCartAgent";

class CustomerClass {
    authGuard = new AuthGuardContainer();
    customerState = new CustomerContainer();
    addressCollectionState = new AddressCollection();

    load() {
        if (this.authGuard.state.authed === null) {
            if (this.authGuard.hasOauthToken()) {
                return this._fetchMyself(true);
            } else {
                return this._requestGuest();
            }
        }
    }

    isFullLogin() {
        return this.authGuard.state.authed === true;
    }

    _requestGuest() {
        HttpClient.put(API_ENDPOINTS.customer.guest).then(resp => {
            if (resp.status === 200) {
                this.authGuard.onTokenChanged(resp.data);
                this._fetchMyself(false);
            }
            return resp;
        });
    }

    _fetchMyself(tryReresh) {
        //HttpClient.withMessage('Fetching User Details').
        HttpClient.get(API_ENDPOINTS.customer.me).then(resp => {
            if (resp && resp.status === 200) {
                this.customerState.setCustomer(resp.data);
                if (!resp.data.is_guest) {
                    this.authGuard.onLogin();
                }
                ShoppingCartAgent.init();
                this._fetchAddresses();
            } else {
                if (tryReresh && resp && resp.status === 401) {
                    //refresh token
                    this._refreshToken()
                }
            }
            return resp;
        });
    }

    _fetchAddresses() {
        // HttpClient.withMessage('Fetching User Address').
        HttpClient.get(API_ENDPOINTS.customer.address).then(resp => {
            if (resp && resp.status === 200) {
                this.addressCollectionState.apply(resp.data);
            }
            return resp;
        });
    }

    login(email, password) {
        return HttpClient.withMessage('Logging on').post(API_ENDPOINTS.oauth2.token, {
            username: email,
            password: password
        }).then(resp => {
            if (resp && resp.status === 200) {
                this.authGuard.onTokenChanged(resp.data);
                this.authGuard.onLogin();
                this._fetchMyself(false);
            } else {
                this.authGuard.onLogout();
                this.customerState.setGuest();
            }
            return resp;
        });
    }

    _refreshToken() {
        return HttpClient.post(API_ENDPOINTS.oauth2.refresh,
                this.authGuard.getRefreshToken())
            .then(resp => {
                if (resp && resp.status === 200) {
                    this.authGuard.onTokenChanged(resp.data);
                    this._fetchMyself(false);
                } else {
                    this._requestGuest();
                }
                return resp;
            });
    }

    register(email, password, firstname, middlename, lastname) {
        return HttpClient.withMessage('Registering').post(API_ENDPOINTS.oauth2.register, {
            username: email,
            password: password,
            firstname: firstname,
            middlename: middlename,
            lastname: lastname
        }).then(resp => {
            if (resp.status === 200) {
                this.authGuard.onLogin(resp.data);
                this._fetchMyself(false);
                GA.event({
                    category: 'User',
                    action: 'Created an Account'
                });
            } else {
                this.authGuard.onLogout();
                this.customerState.setGuest();
            }
            return resp;
        });
    }

    logout() {
        return HttpClient.withMessage('Logging out').delete(API_ENDPOINTS.oauth2.logout).then(
            resp => {
                if (resp.status === 200) {
                    this.authGuard.onDeleteToken();
                    return this._requestGuest();
                }
                return resp;
            }
        );
    }

    addAddress(address) {
        return HttpClient.withMessage('Adding Address').post(API_ENDPOINTS.customer.address, address).then(
            resp => {
                if (resp.status === 201) {
                    this.addressCollectionState.addAddress(resp.data);
                }
                return resp;
            }
        );
    }

    getDefaultShippingAddress() {
        return this.addressCollectionState.getAddressById(
            this.customerState.default_shipping_address_id
        );
    }

    setDefaultShippingAddress(address: CustomerAddress) {}

    setDefaultBillingAddress(address: CustomerAddress) {}
}

let Customer = withPrivateProtect(CustomerClass);
export default Customer;
