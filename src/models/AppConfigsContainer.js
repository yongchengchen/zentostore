// @flow

/**
 * @author yongcheng.chen@live.com
 */

import HttpClient from "./HttpClient";
import {
    API_ENDPOINTS
} from "../consts";
import {
    Container
} from "unstated";

class AppConfigsContainerClass extends Container < {} > {
    state = {};
    load() {
        if (window.zento.configs) {
            this.setState(window.zento.configs);
            return new Promise((resolve, reject) => {
                resolve([]);
            });
        } else {
            return HttpClient.get(API_ENDPOINTS.AppConfigs).then(resp => {
                if (resp.status === 200) {
                    this.setState(resp.data);
                }
                return resp;
            });
        }
    }
}

const AppConfigsContainer = new AppConfigsContainerClass();
export default AppConfigsContainer;
