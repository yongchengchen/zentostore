// @flow

import {
  Container
} from "unstated";

type SORT_BY = {
  code: string,
  title: string
};
type UIState = {
  per_page: number,
  sort_by: string,
  display_mode: string,
  sort_bys: Array < SORT_BY > ,
  display_modes: Array < string > ,
  per_pages: Array < number >
};

const STORAGE_KEY = "uisettings";

const DEFAULT_SETTING = {
  per_page: 9,
  sort_by: "position,asc",
  display_mode: "list",
  sort_bys: [{
      code: "position,asc",
      title: "Default"
    },
    {
      code: "price,asc",
      title: "Price-Lowest First"
    },
    {
      code: "price,desc",
      title: "Price-Highest First"
    },
    {
      code: "name,asc",
      title: "Product Name A-Z"
    },
    {
      code: "name,desc",
      title: "Product Name Z-A"
    },
    {
      code: "discount,desc",
      title: "Highest % Discount"
    }
  ],
  display_modes: ["list", "table"],
  per_pages: [9, 15, 30, 60]
};

class PageSettingCenterClass extends Container < UIState > {
  state = DEFAULT_SETTING;

  init() {
    const settings = localStorage.getItem(STORAGE_KEY);
    if (settings) {
      this.setState(JSON.parse(settings));
    }
  }

  getPerpage() {
    return this.state.per_page;
  }

  setPerPage(per_page: number) {
    this.setState({
      per_page: per_page
    });
    localStorage.setItem(STORAGE_KEY, JSON.stringify(this.state));
  }

  getSortBy() {
    return this.state.sort_by;
  }

  setSortBy(sort_by: string) {
    this.setState({
      sort_by: sort_by
    });
    localStorage.setItem(STORAGE_KEY, JSON.stringify(this.state));
  }
}

const PageSettingCenter = new PageSettingCenterClass();
PageSettingCenter.init();
export default PageSettingCenter;