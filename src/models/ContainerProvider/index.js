// @flow

/**
 * @author yongcheng.chen@live.com
 */

import {
  withPrivateProtect
} from "../../support/helper"

import {
  StateContainer
} from "./StateContainer"

class ContainerProviderClass {
  states = {};

  state(name: string, initValue: any) {
    let container = this.states[name];
    if (container) {
      return container;
    }

    container = new StateContainer(initValue);
    this.states[name] = container;
    return container;
  }
}

let ContainerProvider = withPrivateProtect(ContainerProviderClass);
export default ContainerProvider;