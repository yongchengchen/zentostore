// @flow

import {
  Container
} from "unstated";

type StateContainerModel = {
  value: any
}

export class StateContainer extends Container < StateContainerModel > {
  constructor(initValue: any) {
    super();
    this.state.value = initValue;
  }

  state = {
    value: null
  };

  value(newValue: any) {
    if (newValue === undefined) {
      return this.state.value;
    }
    this.setState({
      value: newValue
    })
  }

  mergeValue(newValue: any) {
    this.setState({
      value: Object.assign({}, this.state.value, newValue)
    })
  }

  toggle() {
    this.setState({
      value: !this.state.value
    })
  }
}