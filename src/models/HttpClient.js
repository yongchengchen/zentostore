/**
 * @author yongcheng.chen@live.com
 */

import axios from "axios";
import {
    DeviceUUID
} from "device-uuid";

import {
    Loader
} from "../components";
import {
    withMagicCall
} from "../support/helper";

import {
    OAUTH2_TOKENS_KEY,
    API_ROOT_PATH
} from "../consts";

class HttpClientClass {
    _uuid = "";
    client = null;
    baseUrl = "";
    _message = "";
    _withSpinner = false;
    timeout = 60000;

    constructor(baseUrl, timeout) {
        this.baseUrl = baseUrl;
        this.timeout = timeout;
        let du = new DeviceUUID();
        let rawInfo = du.parse();
        this._uuid = du.get() + "-" + rawInfo.browser;
        this.reInit();
    }

    reInit() {
        this.client = axios.create({
            baseURL: this.baseUrl,
            timeout: this.timeout || 60000,
            headers: this._getHeaders()
        });

        this.client.interceptors.request.use(
            config => {
                config._loader_tag = this._showSpinnerIfNeeded();
                return config;
            },
            function (error) {
                return Promise.reject(error);
            }
        );

        this.client.interceptors.response.use(
            response => {
                let resp = response.data;
                response.status = resp.status;
                response.data = resp.data;
                this._hideSpinnerIfNeeded(response.config._loader_tag);
                return response;
            },
            error => {
                this._hideSpinnerIfNeeded(error.config._loader_tag);
                return Promise.reject(error);
            }
        );
    }

    uuid() {
        return this._uuid;
    }

    withMessage(message) {
        this._message = message;
        return this.withSpinner();
    }

    withSpinner() {
        this._withSpinner = true;
        return HttpClient;
    }


    _showSpinnerIfNeeded() {
        let tag = '';
        if (this._withSpinner) {
            tag = Loader.show(this._message);
        }
        this._withSpinner = false;
        this._message = "";
        return tag;
    }

    _hideSpinnerIfNeeded(tag) {
        if (tag) {
            Loader.hide(tag === "__spinner__" ? "" : tag);
        }
        this._withSpinner = false;
    }

    _getHeaders() {
        return {
            "Content-Type": "application/json",
            "Guest-Uuid": this._uuid,
            authorization: this._getAuthToken()
        };
    }

    _getAuthToken() {
        let str = localStorage.getItem(OAUTH2_TOKENS_KEY);
        if (str && str !== undefined) {
            const oauth2_tokens = JSON.parse(str);
            if (oauth2_tokens.access_token) {
                return oauth2_tokens.token_type + " " + oauth2_tokens.access_token;
            }
        }
        return "";
    }

    _getUrl(endpoint) {
        if (endpoint.startsWith("http://") || endpoint.startsWith("https://")) {
            return endpoint;
        }
        return this.options.url + endpoint;
    }

    _httpReq(method, ...params) {
        return this.client[method](...params).catch(error => {
            return error.response || {
                status: 500,
                error: error
            };
        });
    }
}
const HttpClient = withMagicCall(
    new HttpClientClass(API_ROOT_PATH, 30000),
    "_httpReq",
    true
);
export default HttpClient;
