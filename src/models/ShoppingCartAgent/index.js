// @flow
/**
 * @author yongcheng.chen@live.com
 */
import HttpClient from "../HttpClient";
import queryString from "query-string";

import {
    Container
} from "unstated";

import type {
    ShoppingCart
} from "../types";

import {
    withPrivateProtect
} from "../../support/helper";

import {
    API_ENDPOINTS
} from "../../consts";

import CustomerAgent from "../CustomerAgent";

const EMPTY_CART = {
    id: 0,
    guid: "",
    email: "",
    store_id: 0,
    customer_id: 0,
    guest_guid: "",
    mode: 0,
    currency: "AUD",
    applied_rules: "",
    coupon_codes: "",
    client_ip: "",
    status: "-1", // -1 means not init
    items_quantity: 0,
    ship_to_billingaddesss: 0,
    billing_address_id: 0,
    shipping_address_id: 0,
    order_id: 0,
    total_weight: 0,
    tax_amount: 0,
    grand_total: 0,
    shipping_fee: 0,
    handle_fee: 0,
    subtotal: 0,
    subtotal_with_discount: 0,
    total: 0,
    items_count: 0,
    billing_address: null,
    shipping_address: null,
    items: []
};

const EVNET_SHIPPINGADDRESS_CHANGED = "shippingaddresschanged";

export class ShoppingCartAgentClass extends Container < ShoppingCart > {
    state = EMPTY_CART;
    events = {};

    init() {
        this._fetchShoppingCart(false);
    }

    clearCart() {
        this.setState(EMPTY_CART);
    }

    //remove product details in items, for payment capture
    getReducedCartData() {
        const cart = this.state;
        const cartItems = [];
        cart.items.forEach(item => {
            let cartItem = {};
            Object.keys(item).forEach((key) => {
                if (key !== "product") {
                    cartItem[key] = item[key];
                }
            });
            cartItems.push(cartItem)
        })

        const data = {
            items: cartItems
        };
        Object.keys(cart).forEach((key) => {
            if (key !== "items") {
                data[key] = cart[key];
            }
        });
        return data;
    }

    addProduct(product_id: number, quantity: number, options: any) {
        return HttpClient.withMessage('Adding product...').post(API_ENDPOINTS.cart.items, {
            product_id: product_id,
            quantity: quantity,
            options: options
        }).then(resp => {
            if (resp && resp.status === 201) {
                this._fetchShoppingCart(true);
            }
            return resp;
        });
    }

    _fetchShoppingCart(withMsg: boolean) {
        let client = withMsg ? HttpClient.withMessage('Updating cart...') : HttpClient;
        return client.get(API_ENDPOINTS.cart.mime).then(resp => {
            if (resp.status === 200) {
                resp.data.email = this.getEmail(resp.data.email);
                this.setState(resp.data);
            }
            if (resp.status === 404) {
                let emptyCart = EMPTY_CART;
                emptyCart.status = 0;
                this.setState(emptyCart)
            }
            return resp;
        });
    }

    getId() {
        return this.state.id;
    }

    getEmail(email) {
        return email || this.state.email || CustomerAgent.customerState.email || "";
    }

    changeEmail(email) {
        let state = this.state;
        let oldEmail = state.email;
        state.email = email;
        this.setState(state);
        return HttpClient
            .post(API_ENDPOINTS.cart.putEmail, {
                email: email
            }).then(resp => {
                if (!resp || resp.status !== 200) {
                    state.email = oldEmail;
                    this.setState(state);
                }
            })
    }

    deleteCartItem(item_id: number) {
        return HttpClient.withMessage('Deleting Item From Shopping Cart...')
            .delete([API_ENDPOINTS.cart.items, item_id].join('/'))
            .then(
                resp => {
                    if (resp.status === 200) {
                        this._fetchShoppingCart();
                    }
                    return resp;
                }
            );
    }

    updateCartItemQty(item_id: number, qty: number) {
        let item = this._findCartItem(item_id);
        let oldQty = item.quantity;
        item.quantity = qty;
        //let UI render directly for input element
        this.setState({
            items: this.state.items
        });

        return HttpClient.withMessage('Update item quantity...').patch(
            [API_ENDPOINTS.cart.items, item_id, "quantity", qty].join('/')
        ).then(resp => {
            if (resp.status === 200) {
                this._fetchShoppingCart();
            } else {
                item.quantity = oldQty;
                this.setState({
                    items: this.state.items
                });
            }
            return resp;
        });
    }

    getCurrentShippingAddress() {
        if (!this.state.shipping_address) {
            let address = CustomerAgent.getDefaultShippingAddress();
            if (!address) {
                if (CustomerAgent.addressCollectionState.getAmount() > 0) {
                    address = CustomerAgent.addressCollectionState.getItems()[0];
                }
                if (address) {
                    this.setShippingAddress(address);
                }
            }
        }
        return this.state.shipping_address;
    }

    registerShippingAddressChangedHandler(callback) {
        if (!this.events[EVNET_SHIPPINGADDRESS_CHANGED]) {
            this.events[EVNET_SHIPPINGADDRESS_CHANGED] = [];
        }
        this.events[EVNET_SHIPPINGADDRESS_CHANGED].push(callback);
    }

    setShippingAddress(address: CustomerAddress) {
        if (this.state.shipping_address_id !== address.id) {
            this.setState({
                shipping_address_id: address.id,
                shipping_address: address
            });
            const events = this.events[EVNET_SHIPPINGADDRESS_CHANGED];
            if (events) {
                events.forEach(cb => {
                    cb(address);
                });
            }
        }
    }

    isSelectedShippingAddress(address: CustomerAddress) {
        const current = this.getCurrentShippingAddress();
        //eslint-disable-next-line
        return current ? address.id == current.id : false;
    }

    isSelectedBillingAddress(address: CustomerAddress) {
        return false;
    }

    _findCartItem(item_id: number) {
        let idx = this.state.items.findIndex(item => {
            return item_id === item.id;
        });
        return this.state.items[idx];
    }

    validateShippingInfomation() {
        const address = this.getCurrentShippingAddress();
        const {
            email
        } = this.state;
        if (address && email) {
            return {
                success: true
            };
        }
        return {
            success: false,
            address: address,
            email: email
        }
    }

    applyCoupon(coupon) {
        return HttpClient.withMessage('Applying coupon...').put(["/cart/mine/coupon", coupon].join("/"), {
            cart_id: this.getId()
        }).then(resp => {
            if (resp && resp.status === 200) {

            }
            return resp;
        });
    }

    getCoupon() {
        return this.state.coupon || "";
    }

    getCartItemProduct = cartItem => {
        const {
            product
        } = cartItem;
        if (product) {
            switch (product.type_id) {
                case "configurable":
                    const options = JSON.parse(cartItem.options);
                    if (options && options.item_id) {
                        const swatchProduct = product.configurables.find(item => {
                            return item.id === options.item_id;
                        });
                        if (swatchProduct && options.swatches) {
                            const url_param = queryString.stringify(options.swatches, {
                                arrayFormat: "bracket"
                            })
                            swatchProduct.url_key = product.url_key + "?" + url_param;
                        }
                        return swatchProduct || product;
                    }
                case "simple":
                default:
                    return product;
            }
        }
        return product;
    };

    parseCartItemOptions = cartItem => {
        return cartItem.options ? JSON.parse(cartItem.options) : {};
    };

    // getCoupons() {
    //   return HttpClient.get(
    //     this._genReqPath('mine', "coupons")
    //   );
    // }

    // deleteCoupons() {
    //   return HttpClient.delete(
    //     this._genReqPath('mine', "coupons")
    //   );
    // }

    // getBillingAddress() {
    //   return HttpClient.get(
    //     this._genReqPath('mine', "billing_address")
    //   );
    // }

    // setBillingAddress() {
    //   return HttpClient.post(
    //     this._genReqPath('mine', "billing_address")
    //   );
    // }

    // getShippingAddress() {
    //   return HttpClient.get(
    //     this._genReqPath('mine', "shipping_address")
    //   );
    // }

    // fetchPaymentInfo(force) {
    //     this._asyncFetchData(this._genReqPath('mine', "payment-information"),
    //       Events.PAYMENT_INFO_UPDATED,
    //       force
    //     ).then(resp => {
    //       return resp;
    //     });
    // }

    // estimateShippingMethods(shipping_address) {
    //     return HttpClient.post(
    //       this._genReqPath('mine', "/estimate-shipping-methods"), {
    //         shipping_address: shipping_address
    //       }
    //     ).then(resp => {
    //       return resp;
    //     });
    // }

    // saveShippingInfo(billing_address, ship_to_billingaddesss, shipping_address, shipping_carrier_code, shipping_method_code) {
    //   let addressInformation = {
    //     billing_address: billing_address,
    //     ship_to_billingaddesss: ship_to_billingaddesss,
    //     shipping_address: shipping_address,
    //     shipping_carrier_code: shipping_carrier_code,
    //     shipping_method_code: shipping_method_code
    //   }

    //   return HttpClient.post(this._genReqPath('mine', "shipping-information"), {
    //     addressInformation: addressInformation
    //   });
    // }

    // mergeCart() {
    //   // '/{cart_guid}/to/{to_cart_guid}'
    // }

    // getCustomer() {
    //   return HttpClient.get(
    //     this._genReqPath('mine' + "/customer")
    //   );
    // }

    // setCustomer(customerId) {
    //   return HttpClient.get(
    //     this._genReqPath(
    //       'mine' + `/customer/${customerId}`
    //     )
    //   );
    // }
}

let ShoppingCartAgent = withPrivateProtect(ShoppingCartAgentClass);
// let ShoppingCartAgent = new ShoppingCartAgentClass;
export default ShoppingCartAgent;
