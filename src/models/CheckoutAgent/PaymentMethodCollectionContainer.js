// @flow

import {
  Container
} from "unstated";

import type {
  PaymentMethod
} from "../types"


type PaymentMethods = {
  items: Array < PaymentMethod >
}

export class PaymentMethodCollectionContainer extends Container < PaymentMethods > {
  state = {
    items: []
  };

  getCollection() {
    return this.state.items;
  }

  apply(methods: Array < PaymentMethod > ) {
    this.setState({
      items: methods
    });
  }
}