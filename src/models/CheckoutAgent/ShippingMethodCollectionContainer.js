// @flow

import {
  Container
} from "unstated";

import type {
  ShippingMethod
} from "../types"

type ShippingMethodCollectionState = {
  current: ShippingMethod,
  items: Array < ShippingMethod >
}

export class ShippingMethodCollectionContainer extends Container < ShippingMethodCollectionState > {
  state = {
    current: null,
    items: []
  };

  getCollection() {
    return this.state.items;
  }

  use(method: ShippingMethod) {
    this.setState({
      current: method
    })
  }

  apply(methods: Array < ShippingMethod > ) {
    this.setState({
      items: methods
    });
  }
}