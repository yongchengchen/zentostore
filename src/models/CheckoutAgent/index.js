// @flow

/**
 * @author yongcheng.chen@live.com
 */
import HttpClient from "../HttpClient";

import type {
    CustomerAddress,
    OrderData
} from "../types"

import {
    PaymentMethodCollectionContainer
} from "./PaymentMethodCollectionContainer";

import {
    ShippingMethodCollectionContainer
} from "./ShippingMethodCollectionContainer";

import {
    withPrivateProtect
} from "../../support/helper";

import ContainerProvider from "../ContainerProvider"
import ShoppingCartAgent from "../ShoppingCartAgent"
import GA from "../GA";

export class CheckoutAgentClass {
    shippingMethods = new ShippingMethodCollectionContainer();
    paymentMethods = new PaymentMethodCollectionContainer();
    comment: string = "";
    orderData: OrderData;

    temp_addr = {}; // for address edit

    constructor() {
        ShoppingCartAgent.registerShippingAddressChangedHandler((address) => {
            this.estimateShipment(address);
            this.estimatePayment(address);
        })
    }

    paymentMethodName() {
        return ContainerProvider.state("payment-method");
    }

    paymentMethodComponentRef() {
        return ContainerProvider.state("payment-method-ref");
    }

    estimatingIndicator() {
        return ContainerProvider.state("refreshing-checkout-options", false);
    }

    estimateShipment(address: CustomerAddress) {
        this.estimatingIndicator().value(true);
        return HttpClient.withMessage('Estimating Shipment...').post("/shipment/estimate/carts/mine", {
            shipping_address: address
        }).then(resp => {
            if (resp.status === 200) {
                this.shippingMethods.apply(resp.data);
            }
            this.estimatingIndicator().value(false);
        });
    }

    estimatePayment(address: CustomerAddress) {
        this.estimatingIndicator().value(true);
        return HttpClient.withMessage('Estimating Payment...').post("/payment/estimate", {
            cart: ShoppingCartAgent.getReducedCartData(),
            shipping_address: address
        }).then(resp => {
            if (resp.status === 200) {
                this.paymentMethods.apply(resp.data);
                this.estimatingIndicator().value(false);
            }
        });
    }

    setComment(comment: string) {

    }

    selectShippingMethod(code: string) {

    }

    selectPaymentMethod(code: string) {

    }

    placeOrder() {

    }

    orderCreated(orderData: OrderData) {
        this.orderData = orderData;
        this._GAecommerceTrack();
        ShoppingCartAgent.clearCart();
    }

    _GAecommerceTrack() {
        const {
            order
        } = this.orderData;
        ShoppingCartAgent.state.items.forEach(item => {
            GA.ec("addProduct", {
                id: order.id, // the same as for addItem to connect them
                name: item.name,
                sku: item.sku, // seems to be required
                price: item.row_price,
                // category: item.,
                quantity: item.quantity,
                currency: order.order_currency_code
            });
        });


        GA.ec("setAction", "purchase", {
            id: order.id, // the same as for addItem to connect them
            // affiliation: "alphazento",
            shipping: 0,
            tax: 0,
            revenue: order.total_amount_include_tax,
            currency: order.order_currency_code
        });

        GA.ec("clear");
    }

    getOrderData() {
        return this.orderData;
    }
}

let CheckoutAgent = withPrivateProtect(CheckoutAgentClass);
export default CheckoutAgent;
