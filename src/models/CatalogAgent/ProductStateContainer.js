// @flow

import {
  Container
} from "unstated";

import type {
  Product
} from "../types";

export class ProductStateContainer extends Container < Product > {
  state = {
    active: 0,
    attribute_set_id: 0,
    category_id: 0,
    cost: 0,
    created_at: "",
    description: "",
    has_options: 0,
    id: 0,
    image: "",
    manufacturer: "",
    meta_description: "",
    meta_keyword: "",
    meta_title: "",
    name: "",
    options_container: "",
    price: 0,
    required_options: 0,
    rrp: 0,
    short_description: "",
    sku: "",
    small_image: "",
    status: 0,
    tax_class_id: 0,
    thumbnail: "",
    type_id: "",
    updated_at: "",
    url_key: "",
    url_path: "",
    visibility: 0,
    weight: 0
  };

  temp_data = {};

  storeTempData(key: string, value: any) {
    this.temp_data[key] = value;
  }

  getTempData(key: string) {
    return this.temp_data[key];
  }

  getProduct() {
    return this.state;
  }

  apply(product: Product) {
    this.setState(product);
  }
}