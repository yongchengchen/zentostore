// @flow

/**
 * @author yongcheng.chen@live.com
 */
import HttpClient from "../HttpClient";

import {
    withPrivateProtect
} from "../../support/helper";

import {
    ProductStateContainer
} from "./ProductStateContainer";

import {
    CategoryCollectionStateContainer
} from "./CategoryCollectionStateContainer";

import {
    SearchCriteria
} from "../types";

import {
    SearchCriteriaStateContainer
} from "./SearchResultStateContainer";

export class CatalogAgentClass {
    _categoriesStateContainer = new CategoryCollectionStateContainer();
    _productStateContainer = new ProductStateContainer();
    _searchCriteriaStateContainer = null;

    init(history) {
        this._searchCriteriaStateContainer = new SearchCriteriaStateContainer(history);
    }

    load() {
        this._fetchCategories();
    }

    resetCreteria() {
        this._searchCriteriaStateContainer.reset();
    }

    getFilter(fliterName: string) {
        return this._searchCriteriaStateContainer.getFilter(fliterName);
    }

    getCategoriesContainer() {
        return this._categoriesStateContainer;
    }

    getCategoryById(categoryId) {
        return this._categoriesStateContainer.getCategoryById(categoryId);
    }

    executeQuery(queryStr: string, extraParams: any) {
        let criteria = this._searchCriteriaStateContainer.fromQueryString(queryStr);
        if (extraParams) {
            criteria = Object.assign({}, criteria, extraParams)
        }
        criteria = this._searchCriteriaStateContainer.getFullOverwriteState(criteria);
        this._searchCriteriaStateContainer.setState(criteria, () => {
            this._searchCriteriaStateContainer.execute(true);
        })
        return this._searchCriteriaStateContainer.getResult();
    }

    search(criteria: SearchCriteria, forceExecuteQuery: boolean) {
        this._searchCriteriaStateContainer.setState(criteria, () => {
            this._searchCriteriaStateContainer.execute(forceExecuteQuery);
        })
        return this._searchCriteriaStateContainer.getResult(criteria);
    }

    appendCriteraFilter(filters, isPagination) {
        return this._searchCriteriaStateContainer.appendCriteraFilter(filters, isPagination);
    }

    getProduct(product_id: number) {
        if (this._productStateContainer.state.id !== product_id) {
            HttpClient.withSpinner().get(`/products/${product_id}`).then(resp => {
                if (resp.status === 200) {
                    this._productStateContainer.apply(resp.data);
                }
            });
        }
        return this._productStateContainer;
    }

    /**
     * for swatch option
     */
    findSubProductByOptions = (product, options) => {
        const optionKeys = Object.keys(options);
        let products = product.configurables;
        optionKeys.forEach(key => {
            const value = options[key];
            products = products.filter(subProduct => {
                return subProduct[key] === value;
            });
        });
        return products;
    };

    _fetchCategories() {
        if (window.zento.categories) {
            this._categoriesStateContainer.apply(window.zento.categories);
        } else {
            HttpClient.withSpinner().get("/categories").then(resp => {
                if (resp.status === 200) {
                    this._categoriesStateContainer.apply(resp.data.data ? resp.data.data : []);
                }
                return resp;
            });
        }
    }
}

const CatalogAgent = withPrivateProtect(CatalogAgentClass);
export default CatalogAgent;