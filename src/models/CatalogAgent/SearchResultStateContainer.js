// @flow

import queryString from "query-string";

import {
    Container
} from "unstated";

import HttpClient from "../HttpClient";

import type {
    Product,
    SearchCriteria
} from "../types";

import {
    RouteContainer
} from "../../dynamicroute";

import PageSettingCenter from "../PageSettingCenter";

type AggregateItem = {
    [key: string]: Array < any >
};

type SearchResultState = {
    aggregate: AggregateItem,
    current_page: number,
    data: Array < Product > ,
    first_page_url: string,
    from: number,
    last_page: number,
    last_page_url: string,
    next_page_url: string,
    path: string,
    per_page: number,
    prev_page_url: string,
    to: number,
    total: number,
    hash ? : string,
    loading ? : boolean
};

const EMPTY_CRITERA = {
    text: "",
    category: [],
    sub_categories: [],
    sort_by: PageSettingCenter.getSortBy(),
    per_page: PageSettingCenter.getPerpage(),
    page: 1
};

const EMPTY_SEARCH_RESULT = {
    aggregate: {
        price: [0, 1000]
    },
    current_page: 1,
    data: [],
    first_page_url: "",
    from: 1,
    last_page: 1,
    last_page_url: "",
    next_page_url: "",
    path: "",
    per_page: 100,
    prev_page_url: "",
    to: 1,
    total: 0,
    hash: "",
    loading: false
};

export class SearchResultStateContainer extends Container < SearchResultState > {
    state = EMPTY_SEARCH_RESULT;
    constructor(initPriceRange ? : Array < number > ) {
        super();
        if (initPriceRange && initPriceRange.length === 2) {
            this.state.aggregate.price = initPriceRange;
        }
    }

    reset() {
        this.setState(EMPTY_SEARCH_RESULT);
    }
}

export class SearchCriteriaStateContainer extends Container < SearchCriteria > {
    state = EMPTY_CRITERA;
    _resultStateContainer = new SearchResultStateContainer();
    use_query_search = window.zento.configs && window.zento.configs.use_query_search;
    history = null;

    constructor(history) {
        super();
        this.history = history;
        RouteContainer.subscribe(() => {
            this.reset();
        })
    }

    /**
     * full overwrite state
     * @param {*} state 
     */
    getFullOverwriteState(newstate) {
        newstate = Object.assign({}, EMPTY_CRITERA, newstate);
        const newStateKeys = Object.keys(newstate);
        Object.keys(this.state).forEach(key => {
            if (!newStateKeys.includes(key)) {
                newstate[key] = Array.isArray(this.state[key]) ? [] : null;
            }
        })
        return newstate;
    }

    reset() {
        this._resultStateContainer.reset();
        this.setState(EMPTY_CRITERA);
    }

    getFilter(fliterName: string) {
        const value = this.state[fliterName];
        if (value) {
            if (Array.isArray(value)) {
                return value.slice(0);
            }
            if (typeof (value) === "object") {
                return Object.assign({}, value);
            }
        }
        return value;
    }

    getResult() {
        return this._resultStateContainer;
    }

    appendCriteraFilter(filters, isPagination) {
        let state = Object.assign({}, this.state);
        Object.keys(filters).forEach(key => {
            state[key] = filters[key];
        });
        if (state.page && !isPagination) {
            state.page = 1;
        }
        this.setState(state, () => {
            this.execute();
        });
    }

    execute(forceExecuteQuery: boolean) {
        if (forceExecuteQuery || !this.use_query_search) {
            return this._execute();
        } else {
            this.history.push(RouteContainer.value().req_uri + "?" + this.toAppQueryString());
        }
    }

    _execute() {
        const queryStr = this._toApiQueryString();
        HttpClient.get(`/catalog/search?${queryStr}`).then(resp => {
            if ([200, 301, 404].indexOf(resp.status) >= 0) {
                if (resp.data) {
                    resp.data.loading = false;
                }
                this._resultStateContainer.setState(resp.data);
            }
        });
    }

    fromQueryString(queryStr: string) {
        const parsed = queryString.parse(queryStr, {
            arrayFormat: "bracket"
        });
        let criteria = {};
        if (parsed) {
            Object.keys(parsed).forEach(key => {
                const value = parsed[key];
                if (value && value !== "") {
                    this._perfectQueryStringItem(criteria, key, value);
                }
            });
        }
        return criteria;
    }

    /**
     * for web app using, so will hide main category parameter when browse category page.
     */
    toAppQueryString() {
        let criteria = Object.assign({}, this.state);
        if (criteria.page === 1) {
            delete criteria.page;
        }
        if (criteria.category) {
            delete criteria.category;
        }
        if (!criteria.text) {
            delete criteria.text;
        }
        criteria["sort_by"] = PageSettingCenter.getSortBy();
        criteria["per_page"] = PageSettingCenter.getPerpage();
        return queryString.stringify(criteria, {
            arrayFormat: "bracket"
        });
    }

    /**
     * for api using, so need to put all params.
     */
    _toApiQueryString() {
        let criteria = Object.assign({}, this.state);
        if (criteria.page === 1) {
            delete criteria.page;
        }
        criteria["sort_by"] = PageSettingCenter.getSortBy();
        criteria["per_page"] = PageSettingCenter.getPerpage();
        return queryString.stringify(criteria, {
            arrayFormat: "bracket"
        });
    }

    _perfectQueryStringItem(object: Object, key: string, value: any) {
        const idx = key.includes("[]");
        if (idx > 0) {
            key = key.substring(0, idx);
            value = Array.isArray(value) ? value : [value];

        } else {
            object[key] = value;
        }
    }
}