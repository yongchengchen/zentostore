// @flow

import {
  Container
} from "unstated";

import type {
  Category
} from "../types";

type CategoryCollectionState = {
  items: Array < Category >
};

export class CategoryCollectionStateContainer extends Container < CategoryCollectionState > {
  state = {
    items: []
  };

  collection() {
    return this.state.items;
  }

  apply(categories: Array < Category > ) {
    this.setState({
      items: categories
    });
  }

  getCategoryById(id) {
    id = Number(id);
    return this._findCategory(id, this.state.items);
  }

  _findCategory(id, items) {
    let category = items.find(item => {
      return item.id === id;
    });
    if (!category) {
      for (let item of items) {
        if (item.children_count > 0) {
          category = this._findCategory(id, item.children);
          if (category) {
            return category;
          }
        }
      }
    }
    return category;
  }
}