/**
 * lazy load for google analysis
 * @author yongcheng.chen@live.com
 */
import {
    withMagicCall
} from "../support/helper";

import {
    RouteContainer
} from "../dynamicroute";

class GAClass {
    constructor() {
        this.ReactGA = false;
        this.plugins = [];
        if (window.zento.configs && window.zento.configs.ga) {
            const {
                enabled,
                id,
                plugins
            } = window.zento.configs.ga;
            if (enabled) {
                (async () => {
                    this.ReactGA = await import('react-ga');
                    this.ReactGA.initialize(id);
                    RouteContainer.subscribe(this._onRouteChanged);
                    if (plugins && Array.isArray(plugins)) {
                        plugins.map(plugin => {
                            this.ReactGA.plugin.require(plugin);
                        })
                        this.plugins = plugins;
                    }

                })();
            }
        }
    }

    _onRouteChanged = () => {
        if (this.ReactGA) {
            console.log('tony GA send page view', RouteContainer.value().req_uri)
            this.ReactGA.pageview(RouteContainer.value().req_uri);
        }
    }
    _gacall(method, ...params) {
        if (this.ReactGA) {
            if (this.plugins.includes(method)) {
                this.ReactGA.plugin.execute(method, ...params);
            } else {
                this.ReactGA[method](...params);
            }
        }
    }
}

const GA = withMagicCall(
    new GAClass(),
    "_gacall",
    true
);
export default GA;
