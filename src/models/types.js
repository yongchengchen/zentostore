export type CustomerAddress = {
    id: number,
    customer_id: number,
    firstname: string,
    middlename ? : string,
    lastname: string,
    company ? : string,
    address1: string,
    address2: string,
    city: string,
    country: string,
    postal_code: string,
    state: string,
    phone: string,
    is_active: ? number
};

export type Customer = {
    id: number,
    group_id ? : number,
    firstname: string,
    middlename ? : string,
    lastname: string,
    email: string,
    guest_email: string,
    is_active: ? number,
    is_guest: ? number,
    created_from_portal: number,
    prefix: string,
    suffix: string,
    dob ? : ? Date,
    default_billing_address_id: number,
    default_shipping_address_id: number,
    taxvat: string,
    confirmation: string,
    gender: string,
    failures_num: number,
    lock_expires: ? string,
    default_billing_address ? : CustomerAddress,
    default_shipping_address ? : CustomerAddress
};

export type AuthState = {
    guid: string,
    status: string,
    login_at: Date
};

export type ShoppingCartItem = {
    id: number,
    cart_id: number,
    product_id: number,
    name: string,
    sku: string,
    price: number,
    custom_price: number,
    description: string,
    url: string,
    image: string,
    quantity: number,
    min_quantity: number,
    max_quantity: number,
    duplicatable: number,
    shippable: number,
    taxable: number,
    unit_price: number,
    total_price: number,
    options: Array < string >
};

export type ShoppingCart = {
    id: number,
    guid: string,
    email: string,
    store_id ? : number,
    customer_id: number,
    guest_guid: string,
    mode: number,
    currency: string,
    applied_rules: string,
    coupon_codes: string,
    client_ip: string,
    status: number,
    items_quantity: number,
    ship_to_billingaddesss: number,
    billing_address_id: number,
    shipping_address_id: number,
    order_id: number,
    total_weight: number,
    tax_amount: number,
    grand_total: number,
    shipping_fee: number,
    handle_fee: number,
    subtotal: number,
    subtotal_with_discount: number,
    total: number,
    items_count: number,
    billing_address ? : CustomerAddress,
    shipping_address ? : CustomerAddress,
    items: Array < ShoppingCartItem >
};

export type Category = {
    all_children: string,
    attribute_set_id: number,
    available_sort_by: Array < string > ,
    children: Array < Category > ,
    children_count: number,
    created_at ? : Date,
    custom_apply_to_products ? : string,
    custom_design ? : string,
    custom_design_from ? : string,
    custom_design_to ? : string,
    custom_layout_update ? : string,
    custom_use_parent_settings ? : string,
    description ? : string,
    display_mode: string,
    filter_price_range ? : string,
    hash: string,
    id: number,
    image ? : string,
    include_in_menu: number,
    is_active: number,
    is_anchor: number,
    landing_page ? : string,
    level: number,
    meta_description ? : string,
    meta_keyword ? : string,
    meta_keywords ? : string,
    meta_title ? : string,
    name: string,
    page_layout ? : string,
    parent_id: number,
    path: string,
    path_in_store: string,
    position: number,
    products_count: number,
    sort_by ? : string,
    updated_at ? : Date,
    url_key: string,
    url_path: string
}

export type Product = {
    active: number,
    attribute_set_id: number,
    category_id: number,
    cost: number,
    created_at: string,
    custom_design ? : string,
    custom_layout_update ? : string,
    description: string,
    gift_message_available ? : string,
    has_options: number,
    id: number,
    image: string,
    manufacturer: string,
    meta_description: string,
    meta_keyword: string,
    meta_title: string,
    name: string,
    options_container: string,
    price: number,
    required_options: number,
    rrp: number,
    short_description: string,
    sku: string,
    small_image: string,
    special_from ? : string,
    special_price ? : string,
    special_to ? : string,
    status: number,
    tax_class_id: number,
    thumbnail: string,
    type_id: string,
    updated_at: string,
    url_key: string,
    url_path: string,
    visibility: number,
    weight: number
}

export type ShippingMethod = {
    method_code: string,
    available: boolean,
    title: string,
    description ? : string,
    shipping_fee: number
}

// export type ShippingMethodEstimateResult = {
//   [key: string]: ShippingMethod
// }

export type PaymentMethod = {
    code: string,
    title: string,
    can_order: boolean,
    surchage_fee_rate: number,
    sucharge_fee_mode: string, //0 fix rate, 1, percentage
    accept_card_types: Array < string > ,
    accept_countries: Array < string >
}

export type RouteItem = {
    req_uri ? : string,
    to_uri ? : string,
    is_regx: boolean,
    component ? : string
}

type CriteriaPriceItem = {
    [key: string]: number
}

export type SearchCriteria = {
    category ? : Array < string | number > ,
    sub_categories ? : Array < string | number > ,
    text ? : string,
    price ? : Array < CriteriaPriceItem > ,
    brand ? : Array < string > ,
    dyn_columns ? : {
        [key: string]: Array < string >
    },
    sort_by ? : string,
    per_page: number,
    page: number
}

export type PaymentTransaction = {
    payment_method: string;
    payment_transaction_id: number;
    comment: string;
    total_due: number;
    amount_authorized: number;
    amount_paid: number;
    amount_refunded: number;
    amount_canceled: number;
}

export type Order = {
    store_id: number;
    invoice_no: number;
    status_id: number;
    coupon_code ? : any;
    customer_id: number;
    customer_is_guest: boolean;
    ext_customer_id: number;
    ext_order_id: number;
    customer_note: string;
    applied_rule_ids: string;
    remote_ip: string;
    total_item_count: number;
    cart_address_id: number;
    cart_id: number;
    base_currency_code: string;
    order_currency_code: string;
    updated_at: string;
    created_at: string;
    id: number;
}

export type Paymentdata = {
    method_name: string;
    payment_req_data: string;
    can_create_order: boolean;
    messages: string[];
    success: boolean;
    payment_transaction: PaymentTransaction;
}

export type OrderData = {
    order: Order;
    payment_data: Paymentdata;
}