import React from "react";
import withRoot from "../views/withRoot";

import { CMSComponentRepo, Spinner } from "../components";

import HttpClient from "../models/HttpClient";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      components: {}
    };

    HttpClient.get("/reactapp/cms/home").then(resp => {
      if (resp && resp.status === 200) {
        this.setState({
          loading: false,
          components: resp.data
        });
      }
    });
  }

  render() {
    const { loading, components } = this.state;
    return (
      <>
        <Spinner show={loading} />
        {Object.keys(components).map((key, idx) => {
          const data = components[key];
          const Component = CMSComponentRepo.getComponent(key);
          return Component ? <Component key={idx} data={data} /> : "";
        })}
      </>
    );
  }
}

export default withRoot(Home);
