import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Provider, Subscribe } from "unstated";

import queryString from "query-string";
import Grid from "@material-ui/core/Grid";

import { CatalogAgent } from "../../models";
import ProductImage from "./ProductImage";
import ProductDetail from "./ProductDetail";
import { ProductTab } from "./ProductTab";
import { RouteContainer } from "../../dynamicroute";
import {
  BreadcrumbComponent,
  SwatchRepo,
  CMSComponentRepo
} from "../../components";

const relativeData = {
  items: [
    {
      id: 4,
      attribute_set_id: 15,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-MB05",
      created_at: "2019-01-23 03:28:57",
      updated_at: "2019-01-23 03:28:57",
      status: 1,
      visibility: 4,
      url_key: "wayfarer-messenger-bag",
      image: "/m/b/mb05-black-0.jpg",
      small_image: "/m/b/mb05-black-0.jpg",
      thumbnail: "/m/b/mb05-black-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/m\\/b\\/mb05-black-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: null,
      sale: null,
      options_container: "container2",
      activity: ["Gym", "Overnight", "Travel"],
      style_bags: ["Messenger", "Laptop"],
      material: ["Nylon", "Polyester"],
      strap_bags: ["Adjustable", "Detachable", "Double", "Padded", "Shoulder"],
      features_bags: [
        "Waterproof",
        "Lightweight",
        "Reflective",
        "Laptop Sleeve",
        "Lockable"
      ],
      new: 1,
      performance_fabric: null,
      gender: [],
      category_gear: [],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Wayfarer Messenger Bag",
      description:
        "<p>Perfect for class, work or the gym, the Wayfarer Messenger Bag is packed with pockets. The dual-buckle flap closure reveals an organizational panel, and the roomy main compartment has spaces for your laptop and a change of clothes. An adjustable shoulder strap and easy-grip handle promise easy carrying.</p>\n<ul>\n<li>Multiple internal zip pockets.</li>\n<li>Made of durable nylon.</li>\n</ul>",
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "45.00",
      special_price: null,
      special_from: null,
      special_to: null
    },
    {
      id: 5,
      attribute_set_id: 15,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-MB06",
      created_at: "2019-01-23 03:28:57",
      updated_at: "2019-01-23 03:28:57",
      status: 1,
      visibility: 4,
      url_key: "rival-field-messenger",
      image: "/m/b/mb06-gray-0.jpg",
      small_image: "/m/b/mb06-gray-0.jpg",
      thumbnail: "/m/b/mb06-gray-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/m\\/b\\/mb06-gray-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: null,
      sale: null,
      options_container: "container2",
      activity: ["Travel", "Urban"],
      style_bags: ["Messenger", "Laptop", "Exercise"],
      material: ["Leather", "Nylon", "Suede"],
      strap_bags: ["Adjustable", "Cross Body", "Shoulder", "Single"],
      features_bags: ["Flapover", "Lightweight", "Laptop Sleeve"],
      new: 1,
      performance_fabric: null,
      gender: [],
      category_gear: [],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Rival Field Messenger",
      description:
        '<p>The Rival Field Messenger packs all your campus, studio or trail essentials inside a unique design of soft, textured leather - with loads of character to spare. Two exterior pockets keep all your smaller items handy, and the roomy interior offers even more space.</p>\n<ul>\n<li>Leather construction.</li>\n<li>Adjustable fabric carry strap.</li>\n<li>Dimensions: 18" x 10" x 4".</li>\n</ul>',
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "45.00",
      special_price: null,
      special_from: null,
      special_to: null
    },
    {
      id: 13,
      attribute_set_id: 15,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-WB07",
      created_at: "2019-01-23 03:29:01",
      updated_at: "2019-01-23 03:29:01",
      status: 1,
      visibility: 4,
      url_key: "overnight-duffle",
      image: "/w/b/wb07-brown-0.jpg",
      small_image: "/w/b/wb07-brown-0.jpg",
      thumbnail: "/w/b/wb07-brown-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/w\\/b\\/wb07-brown-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: null,
      sale: null,
      options_container: "container2",
      activity: ["Overnight", "Travel"],
      style_bags: ["Duffel"],
      material: ["Leather", "Nylon", "Polyester"],
      strap_bags: ["Double"],
      features_bags: ["Waterproof", "Lockable"],
      new: 1,
      performance_fabric: null,
      gender: [],
      category_gear: [],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Overnight Duffle",
      description:
        '<p>For long weekends away, camping outings and business trips, the Overnight Duffle can\'t be beat. The dual handles make it a cinch to carry, while the durable waterproof exterior helps you worry less about the weather. With multiple organizational pockets inside and out, it\'s the perfect travel companion.</p>\n<ul>\n<li>Dual top handles.</li>\n<li>W 15" x H 15" x D 9".</li>\n</ul>',
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "45.00",
      special_price: null,
      special_from: null,
      special_to: null
    },
    {
      id: 19,
      attribute_set_id: 11,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-UG05",
      created_at: "2019-01-23 03:29:05",
      updated_at: "2019-01-23 03:29:05",
      status: 1,
      visibility: 4,
      url_key: "go-get-r-pushup-grips",
      image: "/u/g/ug05-gr-0.jpg",
      small_image: "/u/g/ug05-gr-0.jpg",
      thumbnail: "/u/g/ug05-gr-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/u\\/g\\/ug05-gr-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: 1,
      sale: null,
      options_container: "container2",
      activity: ["Athletic", "Gym"],
      style_bags: [],
      material: ["Plastic", "Rubber"],
      strap_bags: [],
      features_bags: [],
      new: 1,
      performance_fabric: null,
      gender: ["Men", "Women", "Unisex"],
      category_gear: ["Exercise"],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Go-Get'r Pushup Grips",
      description:
        "<p>The Go-Get'r Pushup Grips safely provide the extra range of motion you need for a deep-dip routine targeting core, shoulder, chest and arm strength. Do fewer pushups using more energy, getting better results faster than the standard floor-level technique yield.</p>\n<ul>\n<li>Durable foam grips.</li>\n<li>Supportive base.</li>\n</ul>",
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "19.00",
      special_price: null,
      special_from: null,
      special_to: null
    },
    {
      id: 38,
      attribute_set_id: 11,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-MG03",
      created_at: "2019-01-23 03:29:20",
      updated_at: "2019-01-23 03:29:20",
      status: 1,
      visibility: 4,
      url_key: "summit-watch",
      image: "/m/g/mg03-br-0.jpg",
      small_image: "/m/g/mg03-br-0.jpg",
      thumbnail: "/m/g/mg03-br-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/m\\/g\\/mg03-br-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: null,
      sale: null,
      options_container: "container2",
      activity: ["Recreation", "Athletic", "Sports", "Outdoor", "Gym"],
      style_bags: [],
      material: ["Metal", "Plastic", "Silicone"],
      strap_bags: [],
      features_bags: [],
      new: 1,
      performance_fabric: null,
      gender: ["Men", "Women", "Unisex"],
      category_gear: ["Electronic", "Exercise", "Timepiece"],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Summit Watch",
      description:
        '<p>Trek high and low in the attractive Summit Watch, which features a digital LED display with time and date, stopwatch, lap counter, and 3-second backlight. It can also calculate the number of steps taken and calories burned.</p>\n<ul>\n<li>Brushed metal case.</li>\n<li>Water resistant (100 meters).</li>\n<li>Buckle clasp.</li>\n<li>Strap fits 7" - 10".</li>\n<li>1-year limited warranty.</li>\n</ul>',
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "54.00",
      special_price: null,
      special_from: null,
      special_to: null
    },
    {
      id: 39,
      attribute_set_id: 11,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-MG05",
      created_at: "2019-01-23 03:29:21",
      updated_at: "2019-01-23 03:29:21",
      status: 1,
      visibility: 4,
      url_key: "cruise-dual-analog-watch",
      image: "/m/g/mg05-br-0.jpg",
      small_image: "/m/g/mg05-br-0.jpg",
      thumbnail: "/m/g/mg05-br-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/m\\/g\\/mg05-br-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: null,
      sale: null,
      options_container: "container2",
      activity: ["Recreation"],
      style_bags: [],
      material: ["Leather", "Plastic"],
      strap_bags: [],
      features_bags: [],
      new: 1,
      performance_fabric: null,
      gender: ["Men"],
      category_gear: ["Electronic", "Fashion", "Timepiece"],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Cruise Dual Analog Watch",
      description:
        "<p>Whether you're traveling or wish you were, you'll never let time zones perplex you again with the Cruise Dual Analog Watch. The thick, adjustable band promises a comfortable, personalized fit to this classy, modern time piece.</p>\n<ul>\n<li>Two dials.</li>\n<li>Stainless steel case.</li>\n<li>Adjustable leather band.</li>\n</ul>",
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "55.00",
      special_price: null,
      special_from: null,
      special_to: null
    },
    {
      id: 40,
      attribute_set_id: 11,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-MG02",
      created_at: "2019-01-23 03:29:22",
      updated_at: "2019-01-23 03:29:22",
      status: 1,
      visibility: 4,
      url_key: "dash-digital-watch",
      image: "/m/g/mg02-bk-0.jpg",
      small_image: "/m/g/mg02-bk-0.jpg",
      thumbnail: "/m/g/mg02-bk-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/m\\/g\\/mg02-bk-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: null,
      sale: null,
      options_container: "container2",
      activity: ["Athletic", "Sports", "Outdoor", "Gym"],
      style_bags: [],
      material: ["Rubber"],
      strap_bags: [],
      features_bags: [],
      new: 1,
      performance_fabric: null,
      gender: ["Men"],
      category_gear: ["Electronic", "Exercise", "Timepiece"],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Dash Digital Watch",
      description:
        "<p>The Dash Digital Watch will challenge you to push harder and longer. Log workouts by date, average, and segment times, and recharge by setting hydration and nutrition alarms. This watch is styled with a sleek, square face and durable rubber strap for a long life.</p>\n<ul>\n<li>Digital display.</li>\n<li>LED backlight.</li>\n<li>Rubber strap with buckle clasp.</li>\n<li>1-year limited warranty.</li>",
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "92.00",
      special_price: null,
      special_from: null,
      special_to: null
    },
    {
      id: 44,
      attribute_set_id: 11,
      type_id: "simple",
      has_options: 0,
      required_options: 0,
      active: 1,
      sku: "24-WG02",
      created_at: "2019-01-23 03:29:23",
      updated_at: "2019-01-23 03:29:23",
      status: 1,
      visibility: 4,
      url_key: "didi-sport-watch",
      image: "/w/g/wg02-bk-0.jpg",
      small_image: "/w/g/wg02-bk-0.jpg",
      thumbnail: "/w/g/wg02-bk-0.jpg",
      media_gallery: [
        '{"type":"image","value":"\\/w\\/g\\/wg02-bk-0.jpg","label":"Image"}'
      ],
      tax_class_id: 2,
      erin_recommends: null,
      sale: null,
      options_container: "container2",
      activity: ["Athletic", "Gym"],
      style_bags: [],
      material: ["Metal", "Rubber", "Silicone"],
      strap_bags: [],
      features_bags: [],
      new: 1,
      performance_fabric: null,
      gender: ["Women"],
      category_gear: ["Electronic", "Exercise", "Timepiece"],
      color: null,
      size: null,
      price_type: null,
      sku_type: null,
      weight_type: null,
      price_view: null,
      shipment_type: null,
      quantity_and_stock_status: null,
      msrp_display_actual_price_type: null,
      weight: null,
      eco_collection: null,
      pattern: [],
      climate: [],
      style_bottom: [],
      name: "Didi Sport Watch",
      description:
        "<p>The Didi Sport Watch helps you keep your workout plan down to the second. The vertical, digital face looks sleek and futuristic. This watch is programmed with tons of helpful features such as a timer, an alarm clock, a pedometer, and more to help make your excercise more productive.</p>\n<ul>\n<li>Digital display.</li>\n<li>LED backlight.</li>\n<li>Rubber strap with buckle clasp.</li>\n<li>1-year limited warranty.</li>\n</ul>",
      meta_title: null,
      meta_description: null,
      meta_keyword: null,
      rrp: "0.00",
      cost: "0.00",
      price: "92.00",
      special_price: null,
      special_from: null,
      special_to: null
    }
  ]
};

class ProductPage extends Component {
  handleUrlOptions = product => {
    const { location } = this.props;
    const optionsStateContainer = SwatchRepo.getSwatchOptionsState(product);
    let product_options = optionsStateContainer.value();
    const swatch_options = queryString.parse(location.search);
    if (Object.keys(swatch_options).length > 0 && product.has_options) {
      product_options.options = swatch_options;
      let products = CatalogAgent.findSubProductByOptions(
        product,
        swatch_options
      );
      if (products.length > 0) {
        product_options.main_image = products[0].image;
        product_options.media_gallery = products[0].media_gallery;
      }
      optionsStateContainer.mergeValue(product_options);
    }
  };

  render() {
    const product_id = RouteContainer.value().params.id;
    const ProductCarousel = CMSComponentRepo.getComponent("ProductCarousel");
    return (
      <Provider>
        <Subscribe to={[CatalogAgent.getProduct(product_id)]}>
          {container => {
            const product = container.state;
            if (!product || !product.id) {
              return "";
            }
            const swatch_items = SwatchRepo.initSwatchOptions(product);
            this.handleUrlOptions(product);
            return (
              <Grid container>
                <Grid item xs={12} sm={12}>
                  <BreadcrumbComponent product={product} />
                </Grid>
                <Grid item xs={12} sm={7}>
                  <div style={{ marginLeft: "30px", marginRight: "30px" }}>
                    <ProductImage product={product} />
                  </div>
                </Grid>
                <Grid item xs={12} sm={5}>
                  <div style={{ marginLeft: "30px", marginRight: "30px" }}>
                    <ProductDetail
                      product={product}
                      swatch_items={swatch_items}
                    />
                  </div>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <ProductTab product={product} />
                </Grid>
                <Grid item xs={12} sm={12}>
                  <div>Related Products</div>
                  <ProductCarousel data={relativeData} />
                </Grid>
              </Grid>
            );
          }}
        </Subscribe>
      </Provider>
    );
  }
}

export default withRouter(ProductPage);
