import React, { Component } from "react";
import PropTypes from "prop-types";
import Slider from "react-slick";
import { Provider, Subscribe } from "unstated";
import { ContainerProvider } from "../../models";
import { genProductImageUrl } from "../../consts";

function getSliderSettings(media_gallery) {
  return {
    customPaging: i => {
      if (media_gallery[i]) {
        let media = JSON.parse(media_gallery[i]);
        return <img src={genProductImageUrl(media.value)} alt="" />;
      } else {
        return "";
      }
    },
    dots: true,
    dotsClass: "slick-dots slick-thumb",
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
}

export class ProductImage extends Component {
  render() {
    const { product } = this.props;
    return (
      <Provider>
        <Subscribe
          to={[
            ContainerProvider.state(`prod.${product.id}.options`, {
              media_gallery: product.media_gallery,
              main_image: product.image,
              options: {}
            })
          ]}
        >
          {state => {
            let media_gallery =
              state.value().media_gallery || product.media_gallery || [];
            return (
              <Slider {...getSliderSettings(media_gallery)}>
                {media_gallery.map((str, idx) => {
                  let media = JSON.parse(str);
                  return (
                    <div key={idx} className="product-image-container">
                      <img
                        src={genProductImageUrl(media.value)}
                        alt=""
                        style={{
                          marginLeft: "auto",
                          marginRight: "auto",
                          height: "450px"
                        }}
                      />
                    </div>
                  );
                })}
              </Slider>
            );
          }}
        </Subscribe>
      </Provider>
    );
  }
}

ProductImage.propTypes = {
  product: PropTypes.object.isRequired
};

export default ProductImage;
