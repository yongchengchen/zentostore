import React, { Component } from "react";
import { CatalogAgent } from "../../models";

export class QuantitySelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: props.quantity ? props.quantity : 1,
      minimum: props.minimum ? props.minimum : 1,
      maximum: props.maximum ? props.maximum : 9999999
    };
    CatalogAgent.getProduct(this.props.product_id).storeTempData(
      "quantity",
      this.state.quantity
    );
  }

  onInputChange = e => {
    let qty = e.target.value;
    if (qty < this.state.minimum) {
      qty = this.state.minimum;
    }
    if (qty > this.state.maximum) {
      qty = this.state.maximum;
    }
    CatalogAgent.getProduct(this.props.product_id).storeTempData(
      "quantity",
      qty
    );
    this.setState({ quantity: qty });
  };

  onPlus = e => {
    let qty = this.state.quantity + 1;
    if (qty > this.state.maximum) {
      qty = this.state.maximum;
    }
    CatalogAgent.getProduct(this.props.product_id).storeTempData(
      "quantity",
      qty
    );
    this.setState({ quantity: qty });
  };

  onMinus = e => {
    let qty = this.state.quantity - 1;
    if (qty < this.state.minimum) {
      qty = this.state.minimum;
    }
    CatalogAgent.getProduct(this.props.product_id).storeTempData(
      "quantity",
      qty
    );
    this.setState({ quantity: qty });
  };

  render() {
    return (
      <div className="qty-btn d-flex" style={{ fontSize: "20px" }}>
        <div className="quantity">
          <span className="qty-minus" onClick={this.onMinus}>
            <i className="fa fa-minus" aria-hidden="true" />
          </span>
          <input
            type="number"
            className="qty-text"
            name="quantity"
            value={this.state.quantity}
            onChange={this.onInputChange}
            style={{ width: "48px" }}
          />
          <span className="qty-plus" onClick={this.onPlus}>
            <i className="fa fa-plus" aria-hidden="true" />
          </span>
        </div>
      </div>
    );
  }
}
