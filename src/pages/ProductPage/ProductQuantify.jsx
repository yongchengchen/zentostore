import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";

import { CatalogAgent } from "../../models";

export class ProductQuantify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: props.quantity ? props.quantity : 1,
      minimum: props.minimum ? props.minimum : 1,
      maximum: props.maximum ? props.maximum : 9999999
    };
    CatalogAgent.getProduct(this.props.product_id).storeTempData(
      "quantity",
      this.state.quantity
    );
  }

  onInputChange = e => {
    let qty = e.target.value;
    if (qty < this.state.minimum) {
      qty = this.state.minimum;
    }
    if (qty > this.state.maximum) {
      qty = this.state.maximum;
    }
    CatalogAgent.getProduct(this.props.product_id).storeTempData(
      "quantity",
      qty
    );
    const { onInputChange } = this.props;
    if (onInputChange) {
      onInputChange(qty);
    }
    this.setState({ quantity: qty });
  };

  render() {
    return (
      <TextField
        id="standard-number"
        label="Quantity"
        value={this.state.quantity}
        onChange={this.onInputChange}
        type="number"
        InputLabelProps={{
          shrink: true
        }}
        margin="normal"
        variant="outlined"
      />
    );
  }
}
