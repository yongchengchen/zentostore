import React, { Component } from "react";

import { AppConfigsContainer } from "../../models";
import { Provider, Subscribe } from "unstated";

import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import ShareIcon from "@material-ui/icons/Share";
import { SwatchRepo, SwatchAddCartButton } from "../../components";
import { ProductQuantify } from "./ProductQuantify";

class ProductDetail extends Component {
  render() {
    const { product, swatch_items } = this.props;
    const optionsStateContainer = SwatchRepo.getSwatchOptionsState(product);
    return (
      <Grid container spacing={24}>
        <Grid item xs={12} sm={12}>
          <h2 className={"product-name"}>{product.name}</h2>
        </Grid>
        <Grid item xs={12} sm={12}>
          <div>
            <span className={"product-price"}>${product.price}</span>
            <span className={"product-available"}>In Stock</span>
          </div>
        </Grid>
        <Grid item xs={12} sm={12}>
          <div
            dangerouslySetInnerHTML={{ __html: product.short_description }}
          />
        </Grid>

        <Provider>
          <Subscribe to={[AppConfigsContainer, optionsStateContainer]}>
            {container => {
              return Object.keys(
                AppConfigsContainer.state.swatches
                  ? AppConfigsContainer.state.swatches
                  : {}
              ).map((option, idx) => {
                const SwatchComponent = SwatchRepo.getComponent(option);
                return (
                  <Grid key={idx} item xs={12} sm={12}>
                    <SwatchComponent
                      key={idx}
                      swatch_items={swatch_items}
                      parent_id={product.id}
                      options={optionsStateContainer}
                    />
                  </Grid>
                );
              });
            }}
          </Subscribe>
        </Provider>
        <Grid item xs={12} sm={12}>
          <div className="qty-label">
            <ProductQuantify product_id={product.id} quantity={1} />
          </div>
          <SwatchAddCartButton swatch_items={swatch_items} product={product} />
        </Grid>
        <Grid item xs={12} sm={12}>
          <IconButton aria-label="Add to favorites">
            <FavoriteIcon />
          </IconButton>
          <IconButton aria-label="Share">
            <ShareIcon />
          </IconButton>
        </Grid>
      </Grid>
    );
  }
}
export default ProductDetail;
