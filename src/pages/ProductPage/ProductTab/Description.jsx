import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../../theme/producttab";

class Description extends Component {
  render() {
    const { classes, product } = this.props;
    return (
      <Grid container spacing={24} className={classes.tabPane}>
        <Grid item xs={12} sm={12}>
          <div dangerouslySetInnerHTML={{ __html: product.description }} />
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(Description);
