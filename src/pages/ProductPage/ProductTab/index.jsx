import React, { Component } from "react";
import Description from "./Description";
import { Detail } from "./Detail";
import { Review } from "./Review";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";

export class ProductTab extends Component {
  state = {
    selected_tab: 0
  };
  selectTab = (e, value) => {
    this.setState({ selected_tab: value });
  };
  render() {
    const { product } = this.props;
    return (
      <>
        <Tabs
          value={this.state.selected_tab}
          onChange={this.selectTab}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
        >
          <Tab label="Description" />
          <Tab label="Detail" />
          <Tab label="Review" />
        </Tabs>
        <SwipeableViews axis={"x"} index={this.state.selected_tab}>
          <Description
            product={product}
            active={this.state.selected_tab === 0}
          />
          <Detail product={product} active={this.state.selected_tab === 1} />
          <Review product={product} active={this.state.selected_tab === 2} />
        </SwipeableViews>
      </>
    );
  }
}

ProductTab.propTypes = {
  product: PropTypes.object.isRequired
};

export default ProductTab;
