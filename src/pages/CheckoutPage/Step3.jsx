import React from "react";
import { Provider, Subscribe } from "unstated";

import OrderReview from "./orderreview";
import { CheckoutAgent } from "../../models";

class Step3 extends React.Component {
  placeOrder = e => {
    let method = CheckoutAgent.paymentMethodComponentRef().value();
    if (method) {
      method.capturePayment();
    }
  };

  render() {
    return (
      <div className="zento-checkout">
        <OrderReview />
        <div className="button-zentocheckout clearfix">
          <Provider>
            <Subscribe to={[CheckoutAgent.paymentMethodName()]}>
              {container => (
                <>
                  <div
                    id="zentocheckout-place-order-button-container"
                    style={{
                      display:
                        container.value() === "paypalexpress" ? "" : "none"
                    }}
                  />
                  <button
                    type="button"
                    className="btn-proceed-checkout zentocheckout-btn-checkout zentocheckout-place-order"
                    title="Place Order"
                    onClick={this.placeOrder}
                    disabled={container.value() === undefined}
                    style={{
                      display:
                        container.value() === "paypalexpress" ? "none" : ""
                    }}
                  >
                    <span>
                      <span className="title">Place order now</span>
                    </span>
                  </button>
                </>
              )}
            </Subscribe>
          </Provider>
        </div>
        <div id="zentocheckout-error-messages" className="show">
          <p />
        </div>
      </div>
    );
  }
}
export default Step3;
