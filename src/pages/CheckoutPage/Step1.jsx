import React from "react";

import { Provider, Subscribe } from "unstated";

import className from "classnames";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { CustomerAgent, ShoppingCartAgent } from "../../models";
import { deviceSizeType } from "../../support/helper";
import AddressManager from "./address/addressmanager";
import GuestShippingAddress from "./address/guestshippingaddress";

class Step1 extends React.Component {
  render() {
    return (
      <ExpansionPanel
        className={"steps-panel"}
        defaultExpanded={!deviceSizeType(["xs"])}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <div className="zento-checkout">
            <Provider>
              <Subscribe to={[ShoppingCartAgent]}>
                {container => {
                  const ret = container.validateShippingInfomation();
                  console.log("tony", "step1", ret);
                  return (
                    <h3
                      className={className("step-title step_1", {
                        "step-error": !ret.success
                      })}
                    >
                      Shipping Address
                    </h3>
                  );
                }}
              </Subscribe>
            </Provider>
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div className="zento-checkout">
            <div className="address-information">
              <div id="shipping-section">
                <div
                  id="shipping"
                  className="checkout-shipping-address shipping_address"
                >
                  <div className="clear" />
                  <div className="step-content">
                    {CustomerAgent.authGuard ? (
                      <AddressManager />
                    ) : (
                      <GuestShippingAddress />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}
export default Step1;
