import React, { Component } from "react";
import { CustomerAgent, ContainerProvider } from "../../../models";

const INIT_ADDRESS = {
  email: "",
  address1: "",
  address2: "",
  city: "",
  company: "",
  country: "",
  firstname: "",
  lastname: "",
  middlename: "",
  phone: "",
  postal_code: "",
  state: ""
};

//https://bootsnipp.com/snippets/V1zZ

class AddressForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_saving: false,
      address: props.address ? props.address : INIT_ADDRESS
    };
  }

  handleInputChange = event => {
    let address = this.state.address;
    address[event.target.id] = event.target.value;
    this.setState({
      address: address
    });
  };

  handleSave = e => {
    CustomerAgent.addAddress(this.state.address).then(resp => {
      if (resp.status === 201) {
        ContainerProvider.state("checkout-new-addrs").toggle();
      }
    });
  };

  render() {
    return (
      <form className="form form-shipping-address">
        <div id="shipping-new-address-form" className="fieldset address ">
          <div className="field _required two-fields">
            <div className="control">
              <input
                className="input-text"
                required=""
                type="text"
                id="firstname"
                value={this.state.address.firstname}
                onChange={this.handleInputChange}
              />
              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>First Name</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>

          <div className="field _required two-fields last">
            <div className="control">
              <input
                className="input-text"
                required=""
                type="text"
                id="lastname"
                value={this.state.address.lastname}
                onChange={this.handleInputChange}
              />

              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>Last Name</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>

          <div className="field one-field">
            <div className="control">
              <input
                className="input-text"
                required=""
                type="text"
                id="company"
                value={this.state.address.company}
                onChange={this.handleInputChange}
              />

              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>Company(Optional)</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>

          <div className="field _required two-fields">
            <div className="control _with-tooltip">
              <input
                className="input-text"
                required=""
                type="text"
                id="phone"
                value={this.state.address.phone}
                onChange={this.handleInputChange}
              />

              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>Phone Number</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>

          <div className="control">
            <div className="field _required one-field address-input">
              <div className="control">
                <input
                  className="input-text"
                  required=""
                  type="text"
                  id="address1"
                  value={this.state.address.address1}
                  onChange={this.handleInputChange}
                />
                <label className="label">
                  <span>Street Address</span>
                  <em className="required-symbol">*</em>
                </label>
              </div>
            </div>

            <div className="field additional one-field address-input">
              <div className="control">
                <input
                  className="input-text  address-input"
                  required=""
                  type="text"
                  id="address2"
                  value={this.state.address.address2}
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
          </div>

          <div className="field _required one-field">
            <div className="control">
              <input
                className="input-text"
                required=""
                type="text"
                id="postal_code"
                value={this.state.address.postal_code}
                onChange={this.handleInputChange}
              />

              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>Postcode</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>

          <div className="field _required one-field">
            <div className="control">
              <input
                className="input-text"
                required=""
                type="text"
                id="city"
                value={this.state.address.city}
                onChange={this.handleInputChange}
              />

              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>Suburb</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>

          <div className="field _required one-field">
            <div className="control">
              <select
                className="select"
                id="state"
                value={this.state.address.state}
                onChange={this.handleInputChange}
              >
                <option value="">
                  Please select a region, state or province.
                </option>
                <option data-title="ACT" value="ACT">
                  ACT
                </option>
                <option data-title="NSW" value="NSW">
                  NSW
                </option>
                <option data-title="NT" value="NT">
                  NT
                </option>
                <option data-title="QLD" value="QLD">
                  QLD
                </option>
                <option data-title="SA" value="SA">
                  SA
                </option>
                <option data-title="TAS" value="TAS">
                  TAS
                </option>
                <option data-title="VIC" value="VIC">
                  VIC
                </option>
                <option data-title="WA" value="WA">
                  WA
                </option>
              </select>

              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>State</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>

          <div className="field _required one-field">
            <div className="control">
              <select
                className="select"
                id="country"
                value={this.state.address.country}
                onChange={this.handleInputChange}
              >
                <option value=""> </option>
                <option data-title="Australia" value="AU">
                  Australia
                </option>
              </select>

              <span className="highlight" />
              <span className="bar" />

              <label className="label">
                <span>Country</span>
                <em className="required-symbol">*</em>
              </label>
            </div>
          </div>
          <div />
        </div>

        <div className="clear" />
        <div
          className="actions"
          style={this.props.actions ? {} : { display: "none" }}
        >
          <button
            className="action action-save-address zento-checkout-ok-button"
            type="button"
            onClick={this.handleSave}
          >
            <span>Save</span>
          </button>
          <button
            className="action action-hide-popup zento-checkout-edit-button"
            type="button"
            onClick={this.props.toggleHandler}
          >
            <span>Cancel</span>
          </button>
        </div>
      </form>
    );
  }
}

export default AddressForm;
