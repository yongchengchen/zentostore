import React, { Component } from "react";
import AddressForm from "./addressform";
import className from "classnames";
import {
  CustomerAgent,
  ContainerProvider,
  ShoppingCartAgent
} from "../../../models";
import { Provider, Subscribe } from "unstated";

class AddressManager extends Component {
  onAddressSelected = address => e => {
    ShoppingCartAgent.setShippingAddress(address);
  };

  onEmailChange = event => {
    ShoppingCartAgent.changeEmail(event.target.value);
  };

  render() {
    return (
      <React.Fragment>
        <div>
          <div className="fieldset address ">
            <Provider>
              <Subscribe to={[CustomerAgent.customerState, ShoppingCartAgent]}>
                {container => (
                  <div
                    className={className(
                      "field _required email-field one-field",
                      {
                        "not-display": !CustomerAgent.customerState.state
                          .is_guest
                      }
                    )}
                  >
                    <div className="control">
                      <input
                        className="input-text"
                        required={true}
                        type="email"
                        value={ShoppingCartAgent.state.email}
                        onChange={this.onEmailChange}
                      />
                      <span className="bar" />
                      <label className="label">
                        <span>Email Address</span>
                        <em className="required-symbol">*</em>
                      </label>
                      <span
                        className="note"
                        style={{ display: "inline-block" }}
                      >
                        You can create an account after checkout.
                      </span>
                    </div>
                  </div>
                )}
              </Subscribe>
            </Provider>
          </div>
          <div className="field addresses">
            <div className="control">
              <div className="shipping-address-items">
                <Provider>
                  <Subscribe
                    to={[
                      ShoppingCartAgent,
                      CustomerAgent.addressCollectionState
                    ]}
                  >
                    {container => {
                      const collection = CustomerAgent.addressCollectionState;
                      return collection.getItems().map(address => {
                        return (
                          <div
                            key={address.id}
                            className="shipping-address-item selected-item"
                          >
                            <div className="input-wrapper">
                              <input
                                className="radio"
                                type="radio"
                                value={address.id}
                                checked={ShoppingCartAgent.isSelectedShippingAddress(
                                  address
                                )}
                                onChange={this.onAddressSelected(address)}
                              />
                              <div
                                className="input-mask"
                                onClick={this.onAddressSelected(address)}
                              />
                            </div>
                            <div
                              className="address-detail"
                              onClick={this.onAddressSelected(address)}
                            >
                              <h5 className="customer-name">
                                {address.firstname} {address.lastname}
                              </h5>
                              {address.address1} {address.address2},
                              {address.city} {address.state}{" "}
                              {address.postoal_code} {address.country},{" "}
                              {address.phone}
                            </div>
                            <div className="clear" />
                          </div>
                        );
                      });
                    }}
                  </Subscribe>
                </Provider>
              </div>
              <div />
            </div>
          </div>
          <Provider>
            <Subscribe
              to={[ContainerProvider.state("checkout-new-addrs", false)]}
            >
              {toggleState => (
                <button
                  type="button"
                  className="action action-add-new-address zento-checkout-ok-button"
                  style={toggleState.value() ? { display: "none" } : {}}
                  onClick={toggleState.toggle.bind(toggleState)}
                >
                  <span>New Address</span>
                </button>
              )}
            </Subscribe>
          </Provider>
        </div>
        <div className="clear" />
        <Provider>
          <Subscribe
            to={[ContainerProvider.state("checkout-new-addrs", false)]}
          >
            {toggleState => (
              <div
                id="zento-checkout-new-shipping-address"
                style={toggleState.value() ? {} : { display: "none" }}
              >
                <AddressForm
                  actions={true}
                  toggleHandler={toggleState.toggle.bind(toggleState)}
                />
              </div>
            )}
          </Subscribe>
        </Provider>
      </React.Fragment>
    );
  }
}
export default AddressManager;
