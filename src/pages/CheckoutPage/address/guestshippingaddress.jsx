import React, { Component } from "react";
import AddressForm from "./addressform";

class GuestShippingAddress extends Component {
  constructor(props) {
    super(props);
    this.state = { new_addr_toggle: false, ...props };
  }

  toggleNewAddress = e => {
    let toggled = !this.state.new_addr_toggle;
    this.setState({ new_addr_toggle: toggled });
  };

  selectAddress = e => {
    console.log(e.target);
  };

  render() {
    return (
      <div id="zento-checkout-new-shipping-address">
        <AddressForm showemail={true} actions={false} />
        <div className="clear" />
      </div>
    );
  }
}
export default GuestShippingAddress;
