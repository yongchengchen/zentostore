import React, { Component } from "react";

import TextField from "@material-ui/core/TextField";

import { ShoppingCartAgent } from "../../models";
import { Button } from "../../components";

class Coupon extends Component {
  state = {
    code: ShoppingCartAgent.getCoupon(),
    error: false,
    message: ""
  };

  onCodeChanged = e => {
    this.setState({
      code: e.target.value,
      error: false,
      message: "Click Apply to use the coupon."
    });
  };

  applyCoupon = () => {
    const { code } = this.state;
    if (code) {
      ShoppingCartAgent.applyCoupon(code).then(resp => {
        if (resp && resp.status === 200) {
          this.setState({
            error: false,
            message: "Coupon code applied."
          });
        } else {
          this.setState({
            code: "",
            error: true,
            message: "Coupon code is not valid."
          });
        }
      });
    }
  };

  render() {
    const { code, error, message } = this.state;
    return (
      <div className="zentocheckout-discount">
        <TextField
          label="Has a coupon code"
          placeholder="Coupon code"
          multiline={false}
          margin="none"
          style={{ width: "300px" }}
          value={code}
          onChange={this.onCodeChanged}
          error={error}
        />
        <div className="actions-button">
          <Button
            onClick={this.applyCoupon}
            className={"apply-coupon-button"}
            size={"small"}
          >
            Apply
          </Button>
        </div>
        <p>
          <span style={{ color: error ? "red" : "green" }}>{message}</span>
        </p>
      </div>
    );
  }
}

export default Coupon;
