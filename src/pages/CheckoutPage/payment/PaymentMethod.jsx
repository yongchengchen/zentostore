import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import load from "little-loader";
import Radio from "@material-ui/core/Radio";

import {
  ContainerProvider,
  ShoppingCartAgent,
  HttpClient,
  CheckoutAgent
} from "../../../models";
import { cherryPickPublicFunction } from "../../../support/helper";
import CreditCard from "./creditcard";
import Loader from "./popupwindow/loader";

import { PopupWindow } from "./popupwindow";

import "react-credit-cards/lib/styles.scss";

class PaymentMethod extends Component {
  static extModules = {};
  extJs = null;
  state = {
    number: "",
    name: "",
    expiry: "",
    cvc: "",
    focused: true
  };

  onMethodSelected = e => {
    const { configs } = this.props;
    this._initExtJs(configs);
    CheckoutAgent.paymentMethodName().value(configs.name);
    CheckoutAgent.paymentMethodComponentRef().value(this);
  };

  componentDidMount() {
    const { configs } = this.props;
    if (CheckoutAgent.paymentMethodName().value() === configs.name) {
      this._initExtJs(configs);
    }
  }

  isThisMethod = () => {
    const { configs } = this.props;
    return CheckoutAgent.paymentMethodName().value() === configs.name;
  };

  render() {
    const { configs } = this.props;
    return (
      <React.Fragment>
        <Radio
          checked={this.isThisMethod()}
          onChange={this.onMethodSelected}
          value={configs.name}
        />
        <span
          className="payment-name"
          onClick={this.onMethodSelected}
          dangerouslySetInnerHTML={{ __html: configs["title"] }}
        />

        <div
          style={{
            display: this.isThisMethod() ? "block" : "none"
          }}
        >
          <div
            style={{ display: configs["withCards"] === false ? "none" : "" }}
          >
            <CreditCard component_refname={configs.name + "-card-ref"} />
          </div>
          <div dangerouslySetInnerHTML={{ __html: configs["html"] }} />
        </div>
        <br />
        <PopupWindow
          onClose={this.onRedirectWindowClose}
          component_refname={configs.name + "-window-ref"}
        >
          <Loader />
        </PopupWindow>
      </React.Fragment>
    );
  }

  /**
   * retrieve credit card component ref
   */
  getCartComponentRef() {
    const configs = this.props.configs;
    return ContainerProvider.state(configs.name + "-card-ref").value();
  }

  /**
   * retrieve credit card component ref
   */
  getRedirectWindowComponentRef() {
    const configs = this.props.configs;
    return ContainerProvider.state(configs.name + "-window-ref").value();
  }

  /**
   * design for some payment method like paypal, using single button, to retrieve data
   */
  prepaerPayment() {
    console.log("react prepaerPayment", this.getShoppingCartData());
    return this.extJs.prepaerPayment(this.getShoppingCartData());
  }

  getShoppingCartData() {
    return ShoppingCartAgent.getReducedCartData();
  }

  /**
   * capture payment.  Called by checkout page
   */
  capturePayment() {
    if (this.getCartComponentRef().validate()) {
      this.extJs
        .capturePayment(
          this.getShoppingCartData(),
          this.getCartComponentRef().getData()
        )
        .then(resp => {
          if (resp.status !== 200) {
            this.getRedirectWindowComponentRef().closePopupWindow();
            if (this.props.onPaymentError) {
              this.props.onPaymentError(resp.data.messages);
            }
          }
        });
    }
  }

  openRedirectWindow = windowName => {
    this.getRedirectWindowComponentRef().popup(windowName);
  };

  postUrlInRedirectWindow = (url, params) => {
    this.getRedirectWindowComponentRef().postTo(url, params);
  };

  onRedirectWindowClose = result => {
    if (result) {
      this.extJs
        .postPayment(
          result.query,
          undefined,
          ShoppingCartAgent.getReducedCartData()
        )
        .then(resp => {
          this.onOrderPlaced(resp);
        });
    }
  };

  onOrderPlaced = resp => {
    console.log("placeorde", resp);
    if (resp.status === 201) {
      CheckoutAgent.orderCreated(resp.data);
      this.props.history.push("/checkout/success");
    } else {
      this.props.onPaymentError(resp.data.messages);
    }
  };

  /**
   * init external js lib
   * @param {*} configs
   */
  _initExtJs(configs) {
    if (!PaymentMethod.extModules[configs.name]) {
      console.log("_initExtJs configs", configs);
      if (configs["js"]) {
        this._loadDepends(configs["js"]["depends"], 0, () => {
          load(configs["js"]["entry"], () => {
            this.extJs = window[configs.name];
            if (this.extJs) {
              this.extJs.init(
                cherryPickPublicFunction(this, [
                  "prepaerPayment",
                  "getShoppingCartData",
                  "openRedirectWindow",
                  "capturePayment",
                  "postUrlInRedirectWindow",
                  "onOrderPlaced"
                ]),
                HttpClient,
                configs.params,
                "#zentocheckout-place-order-button-container"
              );
            }
          });
        });
      }
      PaymentMethod.extModules[configs.name] = true;
    } else {
      console.log("this.extJs", this.extJs);
      if (!this.extJs) {
        this.extJs = window[configs.name];
        if (this.extJs.reInit) {
          console.log("this.extJs reInit", this.extJs);
          this.extJs.reInit();
        }
      }
    }
  }

  _loadDepends(depends, idx, cb) {
    if (depends && idx < depends.length) {
      let item = depends[idx];
      load(item.src, () => {
        if (item.namespaces && item.namespaces.length > 0) {
          var code = "";
          item.namespaces.forEach(namespace => {
            code += `window.${namespace}=${namespace};`;
          });
          var script = document.createElement("script");
          script.text = code;
          document.body.appendChild(script);
        }
        this._loadDepends(depends, idx + 1, cb);
      });
    } else {
      if (cb) {
        cb();
      }
    }
  }
}
export default withRouter(PaymentMethod);
