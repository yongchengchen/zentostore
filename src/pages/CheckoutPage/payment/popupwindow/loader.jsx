import React, { Component } from "react";
import loadergif from "./loader.gif";

export default class Loader extends Component {
  render() {
    return (
      <div style={{ backGround: "rgba(54, 224, 236, 0.6)" }}>
        <div className="loader">
          <img
            alt="Loading..."
            src={loadergif}
            style={{
              bottom: 0,
              left: 0,
              margin: "auto",
              position: "fixed",
              right: 0,
              top: 0
            }}
          />
          <div
            style={{
              position: "fixed",
              top: "50%",
              textAlign: "center",
              width: "100%",
              marginTop: "20px",
              marginLeft: "auto",
              marginRight: "auto"
            }}
          >
            <div className="loader-tip">
              Prepareing your payment, please wait...
            </div>
          </div>
        </div>
      </div>
    );
  }
}
