import React from "react";
import ReactDOM from "react-dom";
import Guid from "guid";

import { ContainerProvider } from "../../../../models";
import { Loader } from "../../../../components";

export class PopupWindow extends React.PureComponent {
  constructor(props) {
    super(props);
    this.container = document.createElement("div");
    this.name = "popupwindow";
    this.window = null;
    this.windowCheckerInterval = null;
    this.released = false;
    this.result = null;
    this.loaderHandler = 0;
  }

  componentWillUnmount() {
    if (this.window) {
      this.window.close();
    }
  }

  getWindowOptions() {
    let width = 600;
    let height = 400;
    let left = window.top.outerWidth / 2 + window.top.screenX - width / 2;
    let top = window.top.outerHeight / 2 + window.top.screenY - height / 2;
    return `width=${width},height=${height},left=${left},top=${top}`;
  }

  popup(method_name, options) {
    if (this.window && !this.released) {
      return this.window.focus();
    }
    this.loaderHandler = Loader.show(
      "Don’t see the secure Payment browser?<br/> Click here to show the browser.",
      () => {
        if (this.window && !this.released) {
          return this.window.focus();
        }
      }
    );

    this.name = method_name + Guid.create().toString() || this.name;
    options = options || this.getWindowOptions();
    this.window = window.open("", this.name, options);
    if (this.window) {
      this.released = false;
      this.windowCheckerInterval = setInterval(() => {
        if (this.window) {
          let status = this.window[method_name];
          if (status && status.state === "finished") {
            this.result = status;
            this.closePopupWindow();
          }
          console.log("status", status);
        }
        if (!this.window || this.window.closed) {
          window.onbeforeunload = false;
          this.release();
        }
      }, 100);
      ReactDOM.createPortal(this.props.children, this.container);
      this.window.document.body.appendChild(this.container);
      this.window.document.title = method_name;

      window.onbeforeunload = function() {
        return "Payment is processing, please do not leave the page.";
      };
    }
  }

  render() {
    ContainerProvider.state(this.props.component_refname).value(this);
    return ReactDOM.createPortal(this.props.children, this.container);
  }

  postTo(url, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", url);
    form.setAttribute("target", this.name);

    for (var i in params) {
      if (params.hasOwnProperty(i)) {
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = i;
        input.value = params[i];
        form.appendChild(input);
      }
    }
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
  }

  closePopupWindow() {
    if (this.window) {
      this.window.close();
    }
  }

  /**
   * Release the new window and anything that was bound to it.
   */
  release() {
    // This method can be called once.
    if (this.released) {
      return;
    }
    this.released = true;

    Loader.hide(this.loaderHandler, "unmount");

    // Remove checker interval.
    clearInterval(this.windowCheckerInterval);

    // Call any function bound to the `onUnload` prop.
    const { onClose } = this.props;

    if (typeof onClose === "function") {
      console.log("status  onClose", this.result);
      onClose.call(this, this.result);
    }
    this.result = null;
    this.window = null;
  }
}
