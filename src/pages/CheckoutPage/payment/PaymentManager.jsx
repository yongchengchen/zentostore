import React, { Component } from "react";
import { Provider, Subscribe } from "unstated";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { CheckoutAgent } from "../../../models";
import PaymentMethod from "./PaymentMethod";
import { deviceSizeType } from "../../../support/helper";

//https://bootsnipp.com/snippets/V1zZ

class PaymentManager extends Component {
  render() {
    const { onPaymentError } = this.props;
    return (
      <ExpansionPanel
        className={"steps-panel"}
        defaultExpanded={!deviceSizeType(["xs"])}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <div className="zento-checkout">
            <h3 className="step_3">Payment</h3>
            <Provider>
              <Subscribe to={[CheckoutAgent.estimatingIndicator()]}>
                {stateContainer => (
                  <div
                    className="ajax-loader3 ajax-loader-payment"
                    style={{
                      display: stateContainer.value() ? "block" : "none"
                    }}
                  />
                )}
              </Subscribe>
            </Provider>
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div className="payment-section">
            <ol>
              <li className="payment-method">
                <div className="clear" />
                <div className="zentocheckout-payment-methods">
                  <Provider>
                    <Subscribe
                      to={[
                        CheckoutAgent.paymentMethods,
                        CheckoutAgent.paymentMethodName()
                      ]}
                    >
                      {donotuse =>
                        CheckoutAgent.paymentMethods.state.items.map(
                          configs => {
                            console.log(
                              "payment method configs",
                              CheckoutAgent.paymentMethods.state.items
                            );
                            return (
                              <PaymentMethod
                                key={configs.name}
                                configs={configs}
                                onPaymentError={onPaymentError}
                              />
                            );
                          }
                        )
                      }
                    </Subscribe>
                  </Provider>
                </div>
              </li>
            </ol>
            <Provider>
              <Subscribe to={[CheckoutAgent.estimatingIndicator()]}>
                {stateContainer => (
                  <div
                    id="control_overlay_payment"
                    style={{
                      display: stateContainer.value() ? "block" : "none"
                    }}
                  />
                )}
              </Subscribe>
            </Provider>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

export default PaymentManager;
