import React from "react";
import Card from "react-credit-cards";
import { ContainerProvider } from "../../../models";

import {
  formatCreditCardNumber,
  formatCVC,
  formatExpirationDate
} from "./utils";

import CardValidator from "./cardvalidator";

import "react-credit-cards/es/styles-compiled.css";

export default class CreditCard extends React.Component {
  state = {
    number: "",
    name: "",
    expiry: "",
    cvc: "",
    issuer: "",
    focused: "",
    formData: null,
    error: ""
  };

  handleCallback = ({ issuer }, isValid) => {
    if (isValid) {
      this.setState({ issuer });
    }
  };

  handleInputFocus = ({ target }) => {
    this.setState({
      focused: target.name
    });
  };

  handleInputChange = ({ target }) => {
    let clearError = false;
    if (target.name === "number") {
      target.value = formatCreditCardNumber(target.value);
      clearError = true;
    } else if (target.name === "expiry") {
      target.value = formatExpirationDate(target.value);
    } else if (target.name === "cvc") {
      target.value = formatCVC(target.value);
    }

    if (clearError) {
      this.setState({ [target.name]: target.value, erro: "" });
    } else {
      this.setState({ [target.name]: target.value });
    }
  };

  validate() {
    const formData = this.getData();
    let validator = CardValidator.valdateCardNumber(formData.number);
    console.log("validator", validator);
    if (!validator.isValid) {
      this.setState({ error: "Card number is not available." });
    } else {
      this.setState({ error: "" });
    }
    return validator.isValid;
  }

  getData() {
    const formData = [...this.form.elements]
      .filter(d => d.name)
      .reduce((acc, d) => {
        acc[d.name] = d.value;
        return acc;
      }, {});
    return formData;
  }

  handleSubmit = e => {
    // const { issuer } = this.state;
    const formData = this.getData();
    this.setState({ formData });
    return formData;
  };

  render() {
    const { name, number, expiry, cvc, focused, issuer } = this.state;
    ContainerProvider.state(this.props.component_refname).value(this);
    return (
      <div key="Payment">
        <div className="payment-credit-card">
          <Card
            number={number}
            name={name}
            expiry={expiry}
            cvc={cvc}
            focused={focused}
            callback={this.handleCallback}
          />
          <form ref={c => (this.form = c)} onSubmit={this.handleSubmit}>
            <div className="form-group">
              <input
                type="tel"
                name="number"
                className="form-control"
                placeholder="Card Number"
                pattern="[\d| ]{16,22}"
                required
                onChange={this.handleInputChange}
                onFocus={this.handleInputFocus}
              />
            </div>
            <div className="form-group">
              <input
                type="text"
                name="name"
                className="form-control"
                placeholder="Name"
                required
                onChange={this.handleInputChange}
                onFocus={this.handleInputFocus}
              />
            </div>
            <div className="form-group">
              <input
                type="tel"
                name="expiry"
                className="form-control"
                placeholder="MM/YY"
                pattern="\d\d/\d\d"
                required
                onChange={this.handleInputChange}
                onFocus={this.handleInputFocus}
              />
            </div>
            <div className="form-group">
              <input
                type="tel"
                name="cvc"
                className="form-control"
                placeholder="CVC"
                pattern="\d{3,4}"
                required
                onChange={this.handleInputChange}
                onFocus={this.handleInputFocus}
              />
            </div>
            <div className="form-group">
              <span className="credit-card-error">{this.state.error}</span>
            </div>
            <input type="hidden" name="issuer" value={issuer} />
          </form>
        </div>
      </div>
    );
  }
}
