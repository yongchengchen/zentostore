import React, { Component } from "react";
import Radio from "@material-ui/core/Radio";
import { CheckoutAgent } from "../../../models";

class ShipmentItem extends Component {
  onMethodSelected = e => {
    const { item } = this.props;
    CheckoutAgent.shippingMethods.use(item);
  };

  isThisMethod = () => {
    const { item } = this.props;
    const shippingMethods = CheckoutAgent.shippingMethods.state;
    const isCurrent =
      shippingMethods.current &&
      shippingMethods.current.method_code === item.method_code;
    if (!isCurrent) {
      if (CheckoutAgent.shippingMethods.state.items.length === 1) {
        CheckoutAgent.shippingMethods.use(item);
        return true;
      }
    }
    return isCurrent;
  };

  render() {
    const { item } = this.props;
    return (
      <>
        <Radio
          checked={this.isThisMethod()}
          onChange={this.onMethodSelected}
          value={item.name}
        />
        <span
          className="shipment-name"
          onClick={this.onMethodSelected}
          dangerouslySetInnerHTML={{ __html: item["title"] }}
        />
        <span className="zentocheckout-shipping-price">
          ${item.shipping_fee}
        </span>
      </>
    );
  }
}
export default ShipmentItem;
