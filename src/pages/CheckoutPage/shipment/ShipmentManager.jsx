import React, { Component } from "react";
import { Provider, Subscribe } from "unstated";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { CheckoutAgent } from "../../../models";
import { deviceSizeType } from "../../../support/helper";

import ShipmentItem from "./ShipmentItem";

class ShipmentManager extends Component {
  render() {
    return (
      <ExpansionPanel
        className={"steps-panel"}
        defaultExpanded={!deviceSizeType(["xs"])}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <h3 className="step-title step_2" data-role="title">
            Shipping Methods
          </h3>
          <Provider>
            <Subscribe to={[CheckoutAgent.estimatingIndicator()]}>
              {stateContainer => (
                <div
                  className="ajax-loader3 ajax-loader-shipping-method"
                  style={{
                    display: stateContainer.value() ? "block" : "none"
                  }}
                />
              )}
            </Subscribe>
          </Provider>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div className="order-information">
            <ol>
              <li className="shipping-method">
                <div id="checkout-shipping-method-load">
                  <Provider>
                    <Subscribe to={[CheckoutAgent.shippingMethods]}>
                      {shippingMethods => {
                        return shippingMethods.state.items.map((item, idx) => {
                          return <ShipmentItem key={idx} item={item} />;
                        });
                      }}
                    </Subscribe>
                  </Provider>
                </div>
              </li>
            </ol>
            <Provider>
              <Subscribe to={[CheckoutAgent.estimatingIndicator()]}>
                {stateContainer => (
                  <div
                    id="control_overlay_shipping"
                    style={{
                      display: stateContainer.value() ? "block" : "none"
                    }}
                  />
                )}
              </Subscribe>
            </Provider>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

export default ShipmentManager;
