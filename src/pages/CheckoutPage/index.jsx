import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";

import { ShoppingCartAgent } from "../../models";
import { BreadcrumbComponent } from "../../components";

import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";

import withMq from "../../support/withMq";

class CheckoutPage extends Component {
  componentWillMount() {
    ShoppingCartAgent.subscribe(this.routeGuard);
  }

  componentDidMount() {
    if (
      ShoppingCartAgent.state.status !== "-1" &&
      ShoppingCartAgent.state.items.length === 0
    ) {
      this.props.history.push("/cart");
    }
  }

  componentWillUnmount() {
    ShoppingCartAgent.unsubscribe(this.routeGuard);
  }

  routeGuard = () => {
    if (
      ShoppingCartAgent.state.status !== "-1" &&
      ShoppingCartAgent.state.items.length === 0
    ) {
      this.props.history.push("/cart");
    }
    return "";
  };

  render() {
    return (
      <Grid container>
        <Grid item xs={12}>
          <BreadcrumbComponent extra={{ title: "One Step Checkout" }} />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Step1 />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Step2 />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Step3 />
        </Grid>
      </Grid>
    );
  }
}

export default withMq(withRouter(CheckoutPage));
