import React from "react";

import ShipmentManager from "./shipment/ShipmentManager";
import PaymentManager from "./payment/PaymentManager";
import { Loader } from "../../components";

class Step2 extends React.Component {
  onPaymentError = messages => {
    if (messages) {
      let msgText = "";
      Object.keys(messages).forEach(key => {
        msgText += key + ":" + messages[key] + "\n";
      });
      Loader.error(msgText);
    } else {
      Loader.error("Unknow error happend.");
    }
  };

  render() {
    return (
      <div className="zento-checkout">
        <div className="zentocheckout-shipping-payment-review">
          <ShipmentManager />
          <PaymentManager onPaymentError={this.onPaymentError} />
        </div>
      </div>
    );
  }
}
export default Step2;
