import React, { Component } from "react";
import { Provider, Subscribe } from "unstated";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { deviceSizeType } from "../../support/helper";
import { CheckoutAgent, ShoppingCartAgent } from "../../models";

import CartItemList from "../ShoppingCart/CartItemList";
import OrderSumarry from "../ShoppingCart/OrderSumarry";

import Coupon from "./Coupon";

class OrderReview extends Component {
  handleQtyChange = item_id => value => {
    if (value !== null) {
      CheckoutAgent.estimatingIndicator().value(true);
      ShoppingCartAgent.updateCartItemQty(item_id, value).then(resp => {
        CheckoutAgent.estimatingIndicator().value(false);
      });
    }
  };

  applyCoupon = () => {
    ShoppingCartAgent.applyCoupon();
  };

  render() {
    return (
      <ExpansionPanel
        className={"steps-panel"}
        defaultExpanded={!deviceSizeType(["xs"])}
      >
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <div className="zento-checkout">
            <h3 style={{ float: "left" }} id="review_step_header">
              Order Review
            </h3>
          </div>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div className="zentocheckout-review-info">
            <ol>
              <li className="order-review-info">
                <Provider>
                  <Subscribe to={[CheckoutAgent.estimatingIndicator()]}>
                    {stateContainer => (
                      <div
                        className="ajax-loader3 ajax-loader-order-review"
                        style={{
                          display: stateContainer.value() ? "block" : "none"
                        }}
                      />
                    )}
                  </Subscribe>
                </Provider>

                <div className="clear" />
                <div
                  id="checkout-review-load"
                  className="default-box checkout-review-load"
                >
                  <Provider>
                    <Subscribe to={[ShoppingCartAgent]}>
                      {cart => (
                        <>
                          <CartItemList
                            cartState={cart.state}
                            withTitle={true}
                            imageWidth={"80px"}
                          />
                          <OrderSumarry cart={cart.state} />
                        </>
                      )}
                    </Subscribe>
                  </Provider>
                </div>

                <Coupon />
                <div className="zentocheckout-comment">
                  <textarea
                    col="15"
                    name="billing[zentocheckout_comment]"
                    id="zentocheckout_comment"
                    className="osc-additional-data"
                    placeholder="Leave your comment"
                  />
                </div>

                <div
                  className="ajax-loader3 ajax-loader-order-review"
                  id="ajax-loader3"
                />
                <Provider>
                  <Subscribe to={[CheckoutAgent.estimatingIndicator()]}>
                    {stateContainer => (
                      <div
                        id="control_overlay_review"
                        style={{
                          display: stateContainer.value() ? "block" : "none"
                        }}
                      />
                    )}
                  </Subscribe>
                </Provider>
              </li>
            </ol>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  }
}

export default OrderReview;
