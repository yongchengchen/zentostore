import React, { Component } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import LockIcon from "@material-ui/icons/LockOutlined";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";

import { CustomerAgent } from "../models";

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

class SignIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      message: ""
    };
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
      message: ""
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({
      message: "logining ..."
    });
    CustomerAgent.login(this.state.email, this.state.password).then(resp => {
      if (resp.status === 200) {
        this.props.history.push("/");
      } else {
        if (resp.data && resp.data.message) {
          this.setState({
            message: resp.data.message
          });
        }
      }
    });
  };

  checkFullLogin = () => {
    if (CustomerAgent.isFullLogin()) {
      this.props.history.push("/");
    }
  };

  componentWillMount() {
    CustomerAgent.authGuard.subscribe(this.checkFullLogin);
    this.checkFullLogin();
  }

  componentDidUpdate() {
    this.checkFullLogin();
  }

  componentWillUnmount() {
    CustomerAgent.authGuard.unsubscribe(this.checkFullLogin);
  }

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} onSubmit={this.handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Email Address</InputLabel>
              <Input
                id="email"
                name="email"
                autoComplete="email"
                autoFocus
                value={this.state.email}
                onChange={this.handleChange}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </FormControl>
            {/* <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            /> */}
            <FormControl margin="normal" fullWidth>
              <label style={{ color: "red" }}>{this.state.message}</label>
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign in
            </Button>
            <Typography variant="body2" align="center">
              <NavLink to="/signup" underline="always">
                Sign Up a new account
              </NavLink>
            </Typography>
            <Typography variant="body2" align="center">
              <NavLink to="/forgot-password" underline="always">
                Forgot your password?
              </NavLink>
            </Typography>
          </form>
        </Paper>
      </main>
    );
  }
}

export default withRouter(withStyles(styles)(SignIn));
