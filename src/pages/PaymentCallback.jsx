import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { RouteContainer } from "../dynamicroute";
import queryString from "query-string";

class PaymentCallback extends Component {
  getParams() {
    const parsed = queryString.parse(this.props.location.search);
    return parsed;
  }

  checkIfRedirectBack(method) {
    var script = document.createElement("script");
    script.text =
      `window.${method} = ` +
      '{ state:"finished", success: true, query: "' +
      this.props.location.search +
      '"};';
    document.body.appendChild(script);
    return true;
  }

  render() {
    let method = null;
    const currRoute = RouteContainer.value();
    if (currRoute) {
      // means not using dynamic-route for this route
      method = currRoute.metod;
    } else {
      method = this.props.match.params.method;
    }
    this.checkIfRedirectBack(method);
    return <div />;
  }
}

export default withRouter(PaymentCallback);
