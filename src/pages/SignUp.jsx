import React, { Component } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControl from "@material-ui/core/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Input from "@material-ui/core/Input";
import Checkbox from "@material-ui/core/Checkbox";
import { email, required } from "../components/form/validation";

import InputLabel from "@material-ui/core/InputLabel";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";

import { CustomerAgent } from "../models";

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      message: ""
    };
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value,
      message: ""
    });
  };

  validate = () => {
    const values = this.state;
    const errors = required(
      ["firstname", "lastname", "email", "password"],
      values,
      this.props
    );

    if (!errors.email) {
      const emailError = email(values.email, values, this.props);
      if (emailError) {
        errors.email = email(values.email, values, this.props);
      }
    }
    return errors;
  };

  handleSubmit = event => {
    event.preventDefault();
    const errors = this.validate();
    if (Object.keys(errors).length > 0) {
      this.setState({
        message: Object.values(errors).join(";")
      });
      return;
    }

    this.setState({
      message: "Registering ..."
    });

    CustomerAgent.register(
      this.state.email,
      this.state.password,
      this.state.firstname,
      "",
      this.state.lastname
    ).then(resp => {
      if (resp.status === 200) {
        this.props.history.push("/");
      } else {
        if (resp.data && resp.data.message) {
          this.setState({
            message: resp.data.message
          });
        }
      }
    });
  };

  render() {
    if (CustomerAgent.isFullLogin()) {
      this.props.history.push("/");
    }
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <AccountCircleIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form} onSubmit={this.handleSubmit}>
            <Grid container spacing={16}>
              <Grid item xs={12} sm={6}>
                <FormControl margin="normal" required fullWidth>
                  <InputLabel htmlFor="firstname">First Name</InputLabel>
                  <Input
                    id="firstname"
                    name="firstname"
                    autoComplete="fname"
                    autoFocus
                    value={this.state.firstname}
                    onChange={this.handleChange}
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={6}>
                <FormControl margin="normal" required fullWidth>
                  <InputLabel htmlFor="lastname">Last Name</InputLabel>
                  <Input
                    id="lastname"
                    name="lastname"
                    autoComplete="lname"
                    autoFocus
                    value={this.state.lastname}
                    onChange={this.handleChange}
                  />
                </FormControl>
              </Grid>
            </Grid>

            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Email Address</InputLabel>
              <Input
                id="email"
                name="email"
                autoComplete="email"
                autoFocus
                value={this.state.email}
                onChange={this.handleChange}
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </FormControl>
            <FormControlLabel
              control={<Checkbox value="newsletter" color="primary" />}
              label="Please send me news letter"
            />
            <FormControl margin="normal" fullWidth>
              <label style={{ color: "red" }}>{this.state.message}</label>
            </FormControl>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.submit}
            >
              Sign Up
            </Button>
            <Typography variant="body2" align="center">
              <NavLink to="/signin" underline="always">
                Already have an account?
              </NavLink>
            </Typography>
            <Typography variant="body2" align="center">
              <NavLink to="/forgot-password" underline="always">
                Forgot your password?
              </NavLink>
            </Typography>
          </form>
        </Paper>
      </main>
    );
  }
}

export default withRouter(withStyles(styles)(SignUp));
