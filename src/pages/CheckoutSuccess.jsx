import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import DoneSharpIcon from "@material-ui/icons/DoneSharp";

import { CustomerAgent, CheckoutAgent } from "../models";

const styles = theme => ({
  card: {
    display: "flex",
    textAlign: "center"
  },
  details: {
    display: "flex",
    flexDirection: "column"
  },
  content: {
    flex: "1 0 auto"
  },
  iconButton: {
    marginTop: theme.spacing.unit * 4,
    backgroundColor: "#59AD00",
    "&:hover": {
      backgroundColor: "green"
    }
  },
  doneIcon: {
    height: 38,
    width: 38,
    color: "#fff"
  },
  cover: {
    width: 151
  },
  controls: {
    display: "flex",
    alignItems: "center",
    paddingLeft: theme.spacing.unit,
    paddingBottom: theme.spacing.unit
  },
  email: {
    paddingLeft: theme.spacing.unit,
    color: "blue"
  }
});

class CheckoutSuccess extends Component {
  render() {
    const { classes } = this.props;
    const orderData = CheckoutAgent.getOrderData();
    const customer = CustomerAgent.customerState.state;
    if (!orderData) {
      this.props.history.push("/");
      return "";
    }
    // const { order, payment_data } = orderData;
    const { order } = orderData;
    return (
      <Grid container spacing={8}>
        <Grid item xs={12} sm={12}>
          <Card className={classes.card}>
            <CardContent className={classes.content}>
              <Typography component="h4" variant="h5">
                Your order has been placed
              </Typography>
              <IconButton aria-label="Done" className={classes.iconButton}>
                <DoneSharpIcon className={classes.doneIcon} />
              </IconButton>
              <Typography component="h5" variant="h5">
                Order Number: {order.order_number}
              </Typography>
              <Typography component="p">
                You will receive an email confirmation shortly at
                <span className={classes.email}>
                  {customer.is_guest ? customer.guest_email : customer.email}
                </span>
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Card className={classes.card} />
        </Grid>
      </Grid>
    );
  }
}
CheckoutSuccess.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(CheckoutSuccess);
