import React from "react";
import { NavLink } from "react-router-dom";
import Grid from "@material-ui/core/Grid";

class Notfound404 extends React.Component {
  render() {
    return (
      <Grid container spacing={8}>
        <Grid xs={12} md={4} />
        <Grid xs={12} md={4}>
          <div style={{ margin: "100px 0 100px 20px" }}>
            <p>404</p>
            <p>Oops, we can not find your page.</p>
            <p>
              Please go back to <NavLink to={"/"}>continue shopping</NavLink>.
            </p>
          </div>
        </Grid>
        <Grid xs={12} md={4} />
      </Grid>
    );
  }
}

export default Notfound404;
