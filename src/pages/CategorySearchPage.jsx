import React, { Component } from "react";
import { CatalogAgent, PageSettingCenter } from "../models";
import { Provider, Subscribe } from "unstated";
import {
  CategoryFilter,
  DynamicAttributeFilter,
  ProductList,
  BreadcrumbComponent
} from "../components";

import { RouteContainer } from "../dynamicroute";
import Grid from "@material-ui/core/Grid";
import CssBaseline from "@material-ui/core/CssBaseline";
import Pagination from "material-ui-flat-pagination";

class CategorySearchPage extends Component {
  constructor(props) {
    super(props);
    this.title = "";
  }

  paginate = (e, from) => {
    const page = Math.floor(from / PageSettingCenter.getPerpage()) + 1;
    CatalogAgent.appendCriteraFilter({ page: page }, true);
  };

  search = extraParams => {
    const {
      location: { search }
    } = this.props;

    const category_id = RouteContainer.value().params.id;
    if (category_id) {
      extraParams["category"] = [Number(category_id)];
    }
    return CatalogAgent.executeQuery(search, extraParams);
  };

  getPageTitle = () => {
    const search_text = CatalogAgent.getFilter("text");
    if (search_text) {
      this.title = 'Search results for:"' + search_text + '"';
    } else {
      const category_id = RouteContainer.value().params.id;
      if (category_id) {
        let category = CatalogAgent.getCategoryById(category_id);
        this.title = category ? category.name : "";
      }
    }
  };

  render() {
    const searchResult = this.search({});
    return (
      <Provider>
        <Subscribe to={[searchResult]}>
          {searchResult => {
            let aggregateKeys = Object.keys(searchResult.state.aggregate);
            this.getPageTitle();
            return (
              <Grid container>
                <Grid item xs={12} sm={12}>
                  <BreadcrumbComponent
                    category_id={RouteContainer.value().params.id}
                    extra={
                      RouteContainer.value().params.id
                        ? null
                        : {
                            title: "Search"
                          }
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <h1 className={"category-page-title"}> {this.title}</h1>
                </Grid>
                <Grid item xs={12} sm={3} md={3}>
                  {aggregateKeys.map((key, idx) => {
                    let data = searchResult.state.aggregate[key];
                    let Compo = <div />.type;
                    if (key === "category") {
                      Compo = CategoryFilter;
                    } else {
                      Compo = DynamicAttributeFilter;
                    }
                    return (
                      <Compo
                        key={key}
                        attr_name={key}
                        label={key}
                        data={data}
                      />
                    );
                  })}
                </Grid>
                <Grid item xs={12} sm={9} md={9}>
                  <Grid
                    container
                    style={{
                      display: searchResult.state.loading ? "none" : "block"
                    }}
                  >
                    <Grid item xs={12}>
                      <ProductList paginator={searchResult.state} />
                    </Grid>
                    <Grid item xs={12}>
                      <div style={{ float: "right" }}>
                        <CssBaseline />
                        <Pagination
                          limit={searchResult.state.per_page}
                          offset={searchResult.state.from}
                          total={searchResult.state.total}
                          onClick={this.paginate}
                        />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            );
          }}
        </Subscribe>
      </Provider>
    );
  }
}

export default CategorySearchPage;
