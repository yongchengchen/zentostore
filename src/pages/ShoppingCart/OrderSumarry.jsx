import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  sumarryContainer: {
    paddingTop: 20,
    paddingLeft: 20,
    paddingBottom: 20
  },
  summaryTitle: {
    fontSize: "2em",
    fontWeight: "bold",
    paddingBottom: 20
  },
  hideTitle: {
    display: "none"
  },
  summaryItemTitle: {
    fontSize: "1.2em",
    fontWeight: "bold",
    paddingBottom: 10
  },
  summaryItemValue: {
    fontSize: "1.2em",
    paddingBottom: 10
  }
});

class OrderSumarry extends Component {
  render() {
    const { cart, classes, withTitle } = this.props;
    return (
      <Grid container className={classes.sumarryContainer}>
        <Grid
          item
          xs={12}
          className={withTitle ? classes.summaryTitle : classes.hideTitle}
        >
          Sumarry
        </Grid>
        <Grid item xs={6} className={classes.summaryItemTitle}>
          Cart Subtotal:
        </Grid>
        <Grid item xs={6} className={classes.summaryItemValue}>
          {cart.grand_total}
        </Grid>
        <Grid item xs={6} className={classes.summaryItemTitle}>
          Shipping:
        </Grid>
        <Grid item xs={6} className={classes.summaryItemValue}>
          {cart.shipping_fee}
        </Grid>
        <Grid item xs={6} className={classes.summaryItemTitle}>
          Order Total:
        </Grid>
        <Grid item xs={6} className={classes.summaryItemValue}>
          {cart.subtotal}
        </Grid>
      </Grid>
    );
  }
}
export default withStyles(styles)(OrderSumarry);
