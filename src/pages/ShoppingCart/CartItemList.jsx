import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import NumericInput from "react-numeric-input";
import { withStyles } from "@material-ui/core/styles";

import { ShoppingCartAgent } from "../../models";
import { genProductImageUrl } from "../../consts";

const styles = theme => ({
  imageContainer: {
    textAlign: "center"
  },
  image: {
    width: "80px",
    [theme.breakpoints.up("sm")]: {
      width: "160px"
    }
  },

  itemContent: {
    paddingTop: 10,
    [theme.breakpoints.up("sm")]: {
      paddingTop: 15
    }
  },
  header: {
    height: 15,
    fontWeight: "bold"
  },
  swatchLable: {
    textTransform: "uppercase",
    paddingRight: 5
  },
  swatchTitle: {
    fontWeight: "bold"
  }
});

class CartItemList extends Component {
  handleQtyChange = item_id => value => {
    if (value !== null && value !== undefined) {
      ShoppingCartAgent.updateCartItemQty(item_id, value);
    }
  };

  render() {
    const { cartState, classes, imageWidth } = this.props;
    return (
      <Grid container>
        <Grid item xs={3} />
        <Grid item xs={3}>
          <p className={classes.header}>Product</p>
        </Grid>
        <Grid item xs={3}>
          <p className={classes.header}>Quantity</p>
        </Grid>
        <Grid item xs={3}>
          <p className={classes.header}>Subtotal</p>
        </Grid>
        {cartState.items.map((item, idx) => {
          const product = ShoppingCartAgent.getCartItemProduct(item);
          const { swatches } = ShoppingCartAgent.parseCartItemOptions(item);
          return (
            <Grid item xs={12} key={idx}>
              <Grid container>
                <Grid item xs={3} className={classes.imageContainer}>
                  <NavLink to={product.url_key}>
                    <img
                      className={classes.image}
                      src={genProductImageUrl(product.image)}
                      alt={product.name}
                      style={{ width: imageWidth }}
                    />
                  </NavLink>
                </Grid>
                <Grid item xs={3}>
                  <div className={classes.itemContent}>
                    <div>
                      <NavLink to={product.url_key}>{item.name}</NavLink>
                    </div>
                    {Object.keys(swatches || {}).map(key => {
                      return (
                        <div key={key}>
                          <span className={classes.swatchLable}>{key}:</span>
                          <span className={classes.swatchTitle}>
                            {swatches[key]}
                          </span>
                        </div>
                      );
                    })}
                  </div>
                </Grid>
                <Grid item xs={3}>
                  <div className={classes.itemContent}>
                    <NumericInput
                      mobile
                      value={item.quantity}
                      min={0}
                      max={9999}
                      step={1}
                      style={{
                        input: {
                          width: "95px"
                        }
                      }}
                      onChange={this.handleQtyChange(item.id)}
                    />
                    <div>
                      ${item.price} * {item.quantity}
                    </div>
                  </div>
                </Grid>
                <Grid item xs={3}>
                  <div className={classes.itemContent}>
                    <div>${item.row_price}</div>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    );
  }
}

export default withStyles(styles)(CartItemList);
