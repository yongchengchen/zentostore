import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { NavLink } from "react-router-dom";
import { Spinner } from "../../components";

class EmptyCart extends Component {
  render() {
    const { cartState } = this.props;
    if (cartState.status === "-1") {
      return <Spinner show={true} />;
    }
    return (
      <Grid container>
        <Grid xs={12} md={4} />
        <Grid xs={12} md={4}>
          <div className="cart-empty">
            <div className="el-center">
              <h3>Shopping Cart is Empty</h3>
              <p>You have no items in your shopping cart.</p>
              <p>
                Click <NavLink to="/">here</NavLink> to continue shopping.
              </p>
            </div>
          </div>
        </Grid>
        <Grid xs={12} md={4} />
      </Grid>
    );
  }
}

export default EmptyCart;
