import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Provider, Subscribe } from "unstated";
import Grid from "@material-ui/core/Grid";
import { withStyles } from "@material-ui/core/styles";
import CartItemList from "./CartItemList";
import OrderSumarry from "./OrderSumarry";
import { ShoppingCartAgent } from "../../models";
import EmptyCart from "./EmptyCart";
import { BreadcrumbComponent } from "../../components";

const styles = theme => ({
  container: {
    paddingBottom: theme.spacing.unit * 8
  },
  checkoutButton: {
    width: "100%",
    display: "inline-block",
    backgroundColor: "#f44336",
    color: "#FFFFFF",
    padding: "14px 25px",
    textAlign: "center",
    textDecoration: "none",
    fontSize: "16px",
    marginLeft: "20px",
    opacity: "0.9"
  }
});

class ShoppingCart extends Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.container}>
        <Grid item xs={12} sm={12}>
          <BreadcrumbComponent extra={{ title: "Shopping Cart" }} />
        </Grid>
        <Provider>
          <Subscribe to={[ShoppingCartAgent]}>
            {cart => {
              return cart.state.items.length ? (
                <Grid container>
                  <Grid item xs={12} sm={12} md={9} lg={9}>
                    <CartItemList cartState={cart.state} withTitle={true} />
                  </Grid>
                  <Grid item xs={12} sm={12} md={3} lg={3}>
                    <Grid container>
                      <Grid item xs={12}>
                        <OrderSumarry cart={cart.state} />
                      </Grid>
                      <Grid item xs={11}>
                        <NavLink
                          to="/checkout"
                          className={classes.checkoutButton}
                        >
                          Proceed to Checkout
                        </NavLink>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              ) : (
                <EmptyCart cartState={cart.state} />
              );
            }}
          </Subscribe>
        </Provider>
      </Grid>
    );
  }
}
export default withStyles(styles)(ShoppingCart);
