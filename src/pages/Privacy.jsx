import withRoot from "../modules/withRoot";
// --- Post bootstrap -----
import React from "react";
import Markdown from "../components/Markdown";
import Typography from "../components/Typography";
import LayoutBody from "../components/LayoutBody";
import AppMenuBar from "../modules/views/AppMenuBar";
import privacy from "../modules/views/privacy.md";
import AppFooter from "../modules/views/AppFooter";

function Privacy() {
  return (
    <React.Fragment>
      <AppMenuBar />
      <LayoutBody margin marginBottom>
        <Typography variant="h3" gutterBottom marked="center" align="center">
          Privacy
        </Typography>
        <Markdown>{privacy}</Markdown>
      </LayoutBody>
      <AppFooter />
    </React.Fragment>
  );
}

export default withRoot(Privacy);
