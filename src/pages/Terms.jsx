import withRoot from "../modules/withRoot";
// --- Post bootstrap -----
import React from "react";
import Markdown from "../components/Markdown";
import Typography from "../components/Typography";
import LayoutBody from "../components/LayoutBody";
import AppMenuBar from "../modules/views/AppMenuBar";
import terms from "../modules/views/terms.md";
import AppFooter from "../modules/views/AppFooter";

function Terms() {
  return (
    <React.Fragment>
      <AppMenuBar />
      <LayoutBody margin marginBottom>
        <Typography variant="h3" gutterBottom marked="center" align="center">
          Terms
        </Typography>
        <Markdown>{terms}</Markdown>
      </LayoutBody>
      <AppFooter />
    </React.Fragment>
  );
}

export default withRoot(Terms);
