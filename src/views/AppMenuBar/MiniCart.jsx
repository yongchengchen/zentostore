import React from "react";
import { withRouter } from "react-router-dom";
import { Provider, Subscribe } from "unstated";
import { NavLink } from "react-router-dom";

import Menu from "@material-ui/core/Menu";
import IconButton from "@material-ui/core/IconButton";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Badge from "@material-ui/core/Badge";
import Grid from "@material-ui/core/Grid";

import { ShoppingCartAgent } from "../../models";
import { Button } from "../../components";
import { genProductImageUrl } from "../../consts";
import { withStyles } from "@material-ui/core/styles";
import { styles } from "../../theme/minicart";

class MiniCart extends React.Component {
  state = {
    menuAnchorEl: null
  };

  deleteCartItem = id => e => {
    ShoppingCartAgent.deleteCartItem(id);
  };

  handleMenuOpen = event => {
    this.setState({ menuAnchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ menuAnchorEl: null });
  };

  goToCart = () => {
    this.setState({ menuAnchorEl: null });
    this.props.history.push("/cart");
  };

  goToCheckout = () => {
    this.setState({ menuAnchorEl: null });
    this.props.history.push("/checkout");
  };

  render() {
    const isMenuOpen = Boolean(this.state.menuAnchorEl);
    const { classes } = this.props;
    return (
      <Provider>
        <Subscribe to={[ShoppingCartAgent]}>
          {container => {
            return (
              <>
                <IconButton
                  aria-haspopup="true"
                  onClick={this.handleMenuOpen}
                  color="inherit"
                  className={classes.onlyDesktop}
                >
                  <Badge
                    badgeContent={container.state.items_quantity}
                    color="secondary"
                  >
                    <ShoppingCartIcon />
                  </Badge>
                </IconButton>
                <IconButton
                  aria-haspopup="false"
                  onClick={this.goToCart}
                  color="inherit"
                  className={classes.onlyMobile}
                >
                  <Badge
                    badgeContent={container.state.items_quantity}
                    color="secondary"
                  >
                    <ShoppingCartIcon />
                  </Badge>
                </IconButton>
                <Menu
                  anchorEl={this.state.menuAnchorEl}
                  anchorOrigin={{ vertical: "top", horizontal: "right" }}
                  transformOrigin={{ vertical: "top", horizontal: "right" }}
                  open={isMenuOpen}
                  onClose={this.handleMenuClose}
                  className={classes.onlyDesktop}
                >
                  <Grid
                    container
                    spacing={8}
                    className={classes.container}
                    onClick={this.toggleCart}
                  >
                    {container.state.items_quantity ? (
                      <>
                        <Grid item xs={12} sm={12}>
                          <Grid container spacing={8} className={classes.list}>
                            {container.state.items.map((item, idx) => {
                              const product = ShoppingCartAgent.getCartItemProduct(
                                item
                              );
                              const {
                                swatches
                              } = ShoppingCartAgent.parseCartItemOptions(item);
                              return (
                                <React.Fragment key={idx}>
                                  <Grid item xs={4} sm={4}>
                                    <NavLink
                                      to={product.url_key}
                                      className={classes.image}
                                    >
                                      <img
                                        src={genProductImageUrl(product.image)}
                                        alt="Product"
                                        width="90px"
                                      />
                                    </NavLink>
                                  </Grid>
                                  <Grid item xs={4} sm={4}>
                                    <NavLink to={product.url_key}>
                                      <span style={{ color: "red" }}>
                                        {item.quantity}{" "}
                                      </span>
                                      <span> x </span>
                                      {item.name}
                                    </NavLink>
                                    {Object.keys(swatches || {}).map(
                                      (key, idx) => {
                                        return (
                                          <div key={idx}>
                                            <span
                                              className={classes.swatchLable}
                                            >
                                              {key}:
                                            </span>
                                            <span
                                              className={classes.swatchTitle}
                                            >
                                              {swatches[key]}
                                            </span>
                                          </div>
                                        );
                                      }
                                    )}
                                  </Grid>
                                  <Grid item xs={3} sm={3}>
                                    ${item.row_price}
                                  </Grid>
                                  <Grid item xs={1} sm={1}>
                                    <IconButton
                                      onClick={this.deleteCartItem(item.id)}
                                    >
                                      <DeleteForeverIcon />
                                    </IconButton>
                                  </Grid>
                                </React.Fragment>
                              );
                            })}
                          </Grid>
                        </Grid>

                        <Grid item xs={12} sm={12}>
                          <Grid container spacing={8}>
                            <Grid item xs={3} sm={3}>
                              <span className={classes.summaryTip}>
                                Item(s):
                              </span>
                            </Grid>
                            <Grid item xs={3} sm={3}>
                              {ShoppingCartAgent.state.items_quantity}
                            </Grid>
                            <Grid
                              item
                              xs={3}
                              sm={3}
                              className={classes.summaryTip}
                            >
                              <span className={classes.summaryTip}>
                                Subtotal:
                              </span>
                            </Grid>
                            <Grid item xs={3} sm={3}>
                              ${ShoppingCartAgent.state.subtotal}
                            </Grid>
                          </Grid>
                        </Grid>

                        <Grid item xs={12} sm={12}>
                          <Grid container spacing={8}>
                            <Grid item xs={6} sm={6}>
                              <Button
                                onClick={this.goToCart}
                                className={classes.viewCartButton}
                              >
                                View Cart
                              </Button>
                            </Grid>
                            <Grid item xs={6} sm={6}>
                              <Button
                                onClick={this.goToCheckout}
                                className={classes.checkoutButton}
                              >
                                Checkout
                              </Button>
                            </Grid>
                          </Grid>
                        </Grid>
                      </>
                    ) : (
                      <Grid item xs={12} sm={12}>
                        Cart is empty
                      </Grid>
                    )}
                  </Grid>
                </Menu>
              </>
            );
          }}
        </Subscribe>
      </Provider>
    );
  }
}

export default withRouter(withStyles(styles)(MiniCart));
