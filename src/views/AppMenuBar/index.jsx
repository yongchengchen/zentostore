import React from "react";
import { withRouter, NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { Provider, Subscribe } from "unstated";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import InputBase from "@material-ui/core/InputBase";
import { withStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import { Hidden } from "@material-ui/core";

import { CatalogAgent, CustomerAgent } from "../../models";
import { NavMenu, SearchBar } from "../../components";
import MiniCart from "./MiniCart";
import MyAccountMenu from "./MyAccountMenu";
import logo from "../../logo.png";

import { styles } from "../../theme/appmenubar";

class AppMenuBar extends React.Component {
  render() {
    const { classes, onDrawerToggle } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="sticky" elevation={0}>
          <Toolbar>
            <Hidden mdUp>
              <IconButton
                className={classes.menuButton}
                color="inherit"
                aria-label="Open drawer"
                onClick={onDrawerToggle}
              >
                <MenuIcon />
              </IconButton>
              <IconButton
                className={classes.menuButton}
                color="inherit"
                onClick={onDrawerToggle}
              >
                <SearchIcon />
              </IconButton>
            </Hidden>
            <NavLink to="/">
              <img className={classes.logo} src={logo} alt="Logo" />
            </NavLink>

            <Hidden smDown>
              <Provider>
                <Subscribe to={[CatalogAgent.getCategoriesContainer()]}>
                  {container => {
                    return <NavMenu items={container.collection()} />;
                  }}
                </Subscribe>
              </Provider>
            </Hidden>
            <div className={classes.grow} />
            <Hidden xsDown>
              <SearchBar />
            </Hidden>
            <Provider>
              <Subscribe to={[CustomerAgent.customerState]}>
                {container => <MyAccountMenu customer={container.state} />}
              </Subscribe>
            </Provider>
            <MiniCart />
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

AppMenuBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles)(AppMenuBar));
