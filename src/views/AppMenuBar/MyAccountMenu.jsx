import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import IconButton from "@material-ui/core/IconButton";
import { withRouter } from "react-router-dom";
import { CustomerAgent } from "../../models";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faUserCircle, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
library.add(faUserCircle);

class MyAccountMenu extends React.Component {
  state = {
    menuAnchorEl: null
  };

  handleMenuOpen = event => {
    const { customer } = this.props;
    if (customer.is_guest) {
      this.props.history.push("/signin");
    } else {
      this.setState({ menuAnchorEl: event.currentTarget });
    }
  };

  handleMenuClose = () => {
    this.setState({ menuAnchorEl: null });
  };

  logout = () => {
    this.setState({ menuAnchorEl: null });
    CustomerAgent.logout();
  };

  render() {
    const isMenuOpen = Boolean(this.state.menuAnchorEl);
    const { customer } = this.props;
    return (
      <React.Fragment>
        <IconButton
          aria-owns={isMenuOpen ? "material-appbar" : undefined}
          aria-haspopup="true"
          onClick={this.handleMenuOpen}
          color="inherit"
        >
          {customer.is_guest ? (
            <FontAwesomeIcon icon={faUserCircle} size="1x" />
          ) : (
            // <AccountCircle />
            <FontAwesomeIcon icon={faUser} size="1x" />
          )}
        </IconButton>
        <Menu
          anchorEl={this.state.menuAnchorEl}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          transformOrigin={{ vertical: "top", horizontal: "right" }}
          open={isMenuOpen}
          onClose={this.handleMenuClose}
        >
          <MenuItem onClick={this.handleMenuClose}>My account</MenuItem>
          <MenuItem onClick={this.handleMenuClose}>Order History</MenuItem>
          <MenuItem onClick={this.handleMenuClose}>Tracking</MenuItem>
          <MenuItem onClick={this.handleMenuClose}>My WishList</MenuItem>
          <MenuItem onClick={this.logout}>Logout</MenuItem>
        </Menu>
      </React.Fragment>
    );
  }
}

export default withRouter(MyAccountMenu);
