import React, { Component } from "react";

import Hidden from "@material-ui/core/Hidden";

import { NavigatorDrawer } from "../components";

import withRoot from "./withRoot";
import AppMenuBar from "./AppMenuBar";
import AppFooter from "./AppFooter";

class Layout extends Component {
  state = {
    mobileDrawserOpen: false
  };

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileDrawserOpen: !state.mobileDrawserOpen }));
  };

  render() {
    const { Router } = this.props;
    return (
      <React.Fragment>
        <AppMenuBar onDrawerToggle={this.handleDrawerToggle} />
        <Hidden mdUp implementation="js">
          <NavigatorDrawer
            variant="temporary"
            open={this.state.mobileDrawserOpen}
            onClose={this.handleDrawerToggle}
          />
        </Hidden>
        <Router onUpdate={() => window.scrollTo(0, 0)} />
        <AppFooter />
      </React.Fragment>
    );
  }
}

export default withRoot(Layout);
