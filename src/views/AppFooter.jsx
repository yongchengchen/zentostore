import React from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import pure from "recompose/pure";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { LayoutBody, Typography } from "../components";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faMapMarker,
  faPhone,
  faEnvelope,
  faCreditCard
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { styles } from "../theme/footer";

library.add(faMapMarker, faPhone, faEnvelope, faCreditCard);

function AppFooter(props) {
  const { classes } = props;

  return (
    <Typography component="footer" className={classes.root}>
      <LayoutBody className={classes.layoutBody} width="large">
        <Grid container className={classes.footerContainer}>
          <Grid item xs={6} sm={4} md={3}>
            <Typography variant="h6" gutterBottom className={classes.title}>
              About Us
            </Typography>
            <p className={classes.link}>
              AlphaZento is a better moden e-commerce framework for total
              solution.
            </p>
            <ul className={classes.footerLinks}>
              <li className={classes.listItem}>
                <NavLink to="/" className={classes.link}>
                  <FontAwesomeIcon icon={faMapMarker} size="1x" />
                  Company Address
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  <FontAwesomeIcon icon={faPhone} size="1x" />
                  +61-0288888888
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  <FontAwesomeIcon icon={faEnvelope} size="1x" />
                  yongcheng.chen@live.com
                </NavLink>
              </li>
            </ul>
          </Grid>
          <Grid item xs={6} sm={4} md={3}>
            <Typography variant="h6" gutterBottom className={classes.title}>
              Categories
            </Typography>
            <ul className="footer-links">
              <li className={classes.listItem}>
                <NavLink to="/" className={classes.link}>
                  <i className="fa fa-map-marker" />
                  Hot deals
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  Gear
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  Training
                </NavLink>
              </li>
            </ul>
          </Grid>
          <Grid item xs={6} sm={4} md={3}>
            <Typography variant="h6" gutterBottom className={classes.title}>
              Infomation
            </Typography>
            <ul className="footer-links">
              <li className={classes.listItem}>
                <NavLink to="/" className={classes.link}>
                  <i className="fa fa-map-marker" />
                  Information
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  Gear
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  Training
                </NavLink>
              </li>
            </ul>
          </Grid>
          <Grid item xs={6} sm={4} md={3}>
            <Typography variant="h6" gutterBottom className={classes.title}>
              Service
            </Typography>
            <ul className="footer-links">
              <li className={classes.listItem}>
                <NavLink to="/" className={classes.link}>
                  <i className="fa fa-map-marker" />
                  Hot deals
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  Gear
                </NavLink>
              </li>
              <li className={classes.listItem}>
                <NavLink
                  to="/premium-themes/onepirate/privacy"
                  className={classes.link}
                >
                  Training
                </NavLink>
              </li>
            </ul>
          </Grid>
          <Grid item xs={12} sm={12} md={12}>
            <span className={classes.copyright}>
              Copyright &copy; 2019 All rights reserved.
              <NavLink
                to="https://alphazento.com"
                target="_blank"
                className={classes.link}
              >
                AlphaZento
              </NavLink>
            </span>
          </Grid>
        </Grid>
      </LayoutBody>
    </Typography>
  );
}

AppFooter.propTypes = {
  classes: PropTypes.object.isRequired
};

export default compose(
  pure,
  withStyles(styles)
)(AppFooter);
