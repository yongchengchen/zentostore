import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Loadable from "react-loadable";
import { withSize } from "react-sizeme";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

import AppRouter from "./AppRouter";
import { Loader } from "./components";
import { storeDeviceSize } from "./support/helper";
import PaymentCallback from "./pages/PaymentCallback";

import "./assets/scss/theme.scss";
import "./App.css";

const styles = theme => ({
  container: {
    with: "100%",
    height: "100%",
    textAlign: "center"
  },
  progress: {
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: theme.spacing.unit * 20
  }
});

function CircularIndeterminate(props) {
  const { classes } = props;
  return (
    <div className={classes.container}>
      <CircularProgress className={classes.progress} color="secondary" />
    </div>
  );
}
const loading = withStyles(styles)(CircularIndeterminate);

// Containers
const Layout = Loadable({
  loader: () => import("./views/Layout"),
  loading
});

class App extends Component {
  render() {
    storeDeviceSize(this.props.size);
    return (
      <BrowserRouter>
        <React.Fragment>
          <Loader.Layer />
          <Route path="/paymentcallback/:method" component={PaymentCallback} />
          <Layout Router={AppRouter} />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default withSize()(App);
