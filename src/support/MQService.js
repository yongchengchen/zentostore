/**
 * @author yongcheng.chen@live.com
 */
import {
  Subject
} from "rxjs/Subject";
import {
  is_promise,
  withPrivateProtect
} from "./helper";

class MQServiceClass {
  referer = 0;
  eventListeners = {};
  subscribers = {};

  listenOn(eventName, callback) {
    console.log('listenOn', eventName, callback)
    let listeners = this._getListener(eventName);
    if (!listeners) {
      listeners = new Subject();
      this.eventListeners[eventName] = listeners;
    }
    const subscribe = listeners.asObservable().subscribe(callback);

    let idx = this.referer++;
    const referer = `rf_${idx}`;
    this.subscribers[referer] = subscribe;
    return referer;
  }

  unlisten(referer) {
    if (this.subscribers.hasOwnProperty(referer)) {
      const subscriber = this.subscribers[referer];
      subscriber.unsubscribe();
      delete this.subscribers[referer];
    }
  }

  _getListener(eventName) {
    if (this.eventListeners.hasOwnProperty(eventName)) {
      return this.eventListeners[eventName]
    }
    return false;
  }

  notify(eventName, dataOrCb) {
    let data = dataOrCb;
    if (typeof dataOrCb === "function") {
      data = dataOrCb();
    }
    if (is_promise(data)) {
      data.then(ret => {
        this._notify(eventName, ret);
      });
    } else {
      this._notify(eventName, data);
    }
  }

  _notify(eventName, data) {
    const listener = this._getListener(eventName);
    if (listener) {
      listener.next(data);
    }
  }
}
let MQService = withPrivateProtect(MQServiceClass);
export default MQService;