import React from "react";
import MQService from "./MQService";

export default function withMq(WrappedComponent) {
  return class extends React.Component {
    subsribers = [];

    constructor(props) {
      super(props);
      this.listenOn = this.listenOn.bind(this);
    }

    listenOn(eventName, callback) {
      this.subsribers.push(MQService.listenOn(eventName, callback));
      return this;
    }

    componentWillUnmount() {
      this.subsribers.forEach(referer => {
        MQService.unlisten(referer);
      });
      this.subsribers = [];
    }

    render() {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      return <WrappedComponent listenOn={this.listenOn} {...this.props} />;
    }
  };
}
