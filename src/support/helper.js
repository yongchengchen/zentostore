export function is_promise(object) {
    if (Promise && Promise.resolve) {
        return Promise.resolve(object) === object;
    } else {
        throw new Error("Promise not supported in your environment");
    }
}

export function cherryPickPublicFunction(className, exposeFunctions) {
    let handler = {
        get: function (target, method) {
            if (exposeFunctions.indexOf(method) < 0) {
                throw new Error(`Method [${method} is not exposed to be called.`);
            }
            var origMethod = target[method];
            return typeof origMethod == "function" ?
                origMethod.bind(target) : origMethod;
        }
    };
    return new Proxy(typeof className === "object" ? className : new className(), handler);
}

export function withPrivateProtect(className) {
    let handler = {
        get: function (target, method) {
            if (method[0] === "_" && method !== "_reactFragment") {
                throw new Error("You try to access private function:" + method)
            }
            var origMethod = target[method];
            return typeof origMethod == "function" ?
                origMethod.bind(target) : origMethod;
        }
    };
    return new Proxy(typeof className === "object" ? className : new className(), handler);
}

export function withMagicCall(className, magic_fallback, privateProtected) {
    let handler = {
        get: function (target, method) {
            if (privateProtected === true && method[0] === "_") {
                throw new Error("You try to access private function")
            }
            var origMethod = target[method];
            if (origMethod === undefined) {
                origMethod = target[magic_fallback];
                return origMethod.bind(target, method);
            }
            return typeof origMethod == "function" ?
                origMethod.bind(target) : origMethod;
        }
    }
    return new Proxy(typeof className === "object" ? className : new className(), handler);
}

let deviceSize = {
    width: 1280,
    height: 600
};

export function storeDeviceSize(size) {
    deviceSize.width = size.width;
    deviceSize.height = size.height;
}

// xs (extra-small): 0px or larger
// sm (small): 600px or larger
// md (medium): 960px or larger
// lg (large): 1280px or larger
// xl (extra-large): 1920px or larger
export function deviceSizeType(types) {
    let type = 'xl';
    if (deviceSize.width < 1920) {
        type = 'lg'
    }
    if (deviceSize.width < 1280) {
        type = 'md'
    }
    if (deviceSize.width < 960) {
        type = 'sm'
    }
    if (deviceSize.width < 600) {
        type = 'xs'
    }
    return types.indexOf(type) !== -1;
}