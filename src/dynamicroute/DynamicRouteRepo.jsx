//@flow

import { withPrivateProtect } from "../support/helper";
import { RouteItem } from "../models/types";
import { matchPath } from "react-router-dom";

class DynamicRouteRepoClass {
  routes = {};
  cached_routes = {};
  regxs = [];
  route404 = {};

  register(item: RouteItem) {
    if (item.is_regx) {
      this.regxs.push(item.req_uri);
    } else {
      item.req_uri = this._prefectUri(item.req_uri);
    }
    this.routes[item.req_uri] = item;
  }

  register404(item: RouteItem) {
    this.route404 = item;
  }

  _cache(item: RouteItem) {
    this.cached_routes[this._prefectUri(item.req_uri)] = item;
  }

  getRouteItem(req_uri: string) {
    let url = this._prefectUri(req_uri);
    if (this.routes[url] !== undefined) {
      return this.routes[url];
    }
    if (this.cached_routes[url] !== undefined) {
      return this.cached_routes[url];
    }
    return this._regxMatchRoute(url);
  }

  _regxMatchRoute(req_uri: string) {
    let route = false;
    this.regxs.some(url_regx => {
      let matched = matchPath(req_uri, {
        path: url_regx
      });
      if (matched) {
        const raw = this.routes[url_regx];
        route = {
          path: raw.path,
          req_uri: raw.req_uri,
          name: raw.name,
          component: raw.component,
          params: matched.params,
          helmet: raw.helmet
        };
      }
      return matched;
    });
    return route;
  }

  _prefectUri(uri: string) {
    if (uri.charAt(0) !== "/") {
      uri = "/" + uri;
    }
    return uri;
  }

  getRewriteRouteItem(to_uri: string, origin_url: string) {
    let routeItem = this.getRouteItem(to_uri);
    if (!routeItem) {
      routeItem = this.route404;
    }
    routeItem = this.cloneRouteItemReplaceUrl(routeItem, origin_url);
    this._cache(routeItem);
    return routeItem;
  }

  cloneRouteItemReplaceUrl(routeItem: RouteItem, url: string) {
    // routeItem = JSON.parse(JSON.stringify(routeItem));
    routeItem.req_uri = url;
    return routeItem;
  }
}

let DynamicRouteRepo = withPrivateProtect(DynamicRouteRepoClass);
export default DynamicRouteRepo;
