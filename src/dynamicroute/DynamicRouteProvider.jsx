import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { Spinner } from "../components";
import { HttpClient } from "../models";
import { API_ENDPOINTS } from "../consts";

import DynamicRouteRepo from "./DynamicRouteRepo";
import RouteContainer from "./RouteContainer";

class DynamicRouteProvider extends Component {
  state = {
    showloader: true
  };

  componentDidMount() {
    this.previousPath = "";
    this.tryUrlRewrite();
  }

  componentDidUpdate() {
    this.tryUrlRewrite();
  }

  _requestRewrite(url) {
    if (window.zento.urlrw && window.zento.urlrw[url]) {
      return new Promise((resolve, reject) => {
        resolve(window.zento.urlrw[url]);
      });
    } else {
      return HttpClient.get(API_ENDPOINTS.urlfinder, {
        params: {
          url: url
        }
      });
    }
  }

  tryUrlRewrite() {
    const {
      location: { pathname }
    } = this.props;
    if (this.previousPath !== pathname) {
      this.setState({ showloader: true });
      let routeItem = DynamicRouteRepo.getRouteItem(pathname);
      if (routeItem) {
        this.setState({ showloader: false });
        RouteContainer.value(routeItem);
      } else {
        let url = pathname;
        if (url.charAt(0) === "/") {
          url = url.substr(1);
        }
        this._requestRewrite(url).then(resp => {
          this.setState({ showloader: false });
          if (resp.status === 200) {
            RouteContainer.value(
              DynamicRouteRepo.getRewriteRouteItem(resp.data.to_uri, pathname)
            );
          } else {
            RouteContainer.value(
              DynamicRouteRepo.cloneRouteItemReplaceUrl(
                DynamicRouteRepo.route404,
                pathname
              )
            );
          }
        });
      }
    }
    this.previousPath = pathname;
  }

  render() {
    return <Spinner show={this.state.showloader} />;
  }
}

export default withRouter(DynamicRouteProvider);
