import { withPrivateProtect } from "../support/helper";

/**
 * so backend we can configurate to use these dynamic components
 */
class DynamicComponentRepoClass {
  components = {};
  register(name, component) {
    this.components[name] = component;
  }

  getComponent(name) {
    if (typeof name === "object") {
      return name;
    }
    if (typeof name === "string") {
      return this.components[name];
    }
    return name;
  }

  has(name) {
    return this.components[name];
  }
}

let DynamicComponentRepo = withPrivateProtect(DynamicComponentRepoClass);
export default DynamicComponentRepo;
