import { ContainerProvider } from "../models";

const RouteContainer = ContainerProvider.state("dynamic-route", {});
export default RouteContainer;
