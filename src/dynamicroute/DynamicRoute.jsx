import React, { Suspense } from "react";
import { Route } from "react-router-dom";
import { Helmet } from "react-helmet";
import { withStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import DynamicComponentRepo from "./DynamicComponentRepo";

const styles = theme => ({
  progress: {
    margin: theme.spacing.unit * 2
  }
});

class DynamicRoute extends React.Component {
  render() {
    const { classes, route } = this.props;
    if (route !== undefined && route.req_uri) {
      const { helmet } = route;
      const title = helmet && helmet.title ? <title>{helmet.title}</title> : "";
      const canonical =
        helmet && helmet.canonical ? (
          <link rel="canonical" href={helmet.canonical} />
        ) : (
          ""
        );
      const renderHelmet = helmet ? (
        <Helmet>
          {title}
          {canonical}
        </Helmet>
      ) : (
        ""
      );

      const Component = DynamicComponentRepo.getComponent(
        this.props.route.component
      );
      return Component ? (
        <Suspense fallback={<div />}>
          {renderHelmet}
          <Route
            exact
            path={this.props.route.req_uri}
            render={props => <Component {...props} />}
          />
        </Suspense>
      ) : (
        <div>
          <CircularProgress className={classes.progress} color="secondary" />
        </div>
      );
    } else {
      return <React.Fragment />;
    }
  }
}
export default withStyles(styles)(DynamicRoute);
