import React, { Component } from "react";
import { Provider, Subscribe } from "unstated";
import { Route, withRouter } from "react-router-dom";
import {
  DynamicRoute,
  DynamicRouteRepo,
  DynamicRouteProvider,
  RouteContainer
} from "./dynamicroute";

import { AppConfigsContainer, CustomerAgent, CatalogAgent, GA } from "./models";

import routes from "./routes";

const Notfound404 = React.lazy(() => import("./pages/Notfound404"));

class AppRouter extends Component {
  constructor(props) {
    super(props);
    routes.forEach(item => {
      DynamicRouteRepo.register({
        req_uri: item.path,
        params: [],
        is_regx: item.is_regx,
        component: item.component,
        helmet: item.helmet
      });
    });
    DynamicRouteRepo.register404({
      req_uri: "*",
      params: [],
      is_regx: false,
      component: Notfound404
    });
  }

  inited = false;
  initLogic = () => {
    if (!this.inited) {
      const { history } = this.props;
      this.inited = true;
      CatalogAgent.init(history);
      AppConfigsContainer.load().then(resp => {
        CatalogAgent.load();
        CustomerAgent.load();
      });
    }
  };

  render() {
    this.initLogic();
    return (
      <>
        <Provider>
          <Subscribe to={[RouteContainer]}>
            {container => {
              return <DynamicRoute route={container.value()} />;
            }}
          </Subscribe>
        </Provider>
        <Route path="*" component={DynamicRouteProvider} />
      </>
    );
  }
}

export default withRouter(AppRouter);
